FORMAT: A1

# Media Library

The Media Library APIs helps the users to send, transform and publish some media for public usages.

# Group CfgCdn

Resources related to the configuration models of CDNs.

## CfgCdn Collection [/api/cfg-cdns]

### List all the CfgCdn [GET]

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": "177a0f40-10e2-41df-98c8-cb75ebe990bc",
                    "name": "media-library",
                    "model_schema": {
                        "type": "object",
                        "additionalProperties": false
                    }
                }
            ]
            
    + Schema
        
            {
                "$ref": "./json-schema/collection.cfg-cdn.json"
            }

### Create a new CfgCdn [POST]

+ Request (application/json)

    + Body
    
            {
                "name": "media-library",
                "model_schema": {
                    "type": "object",
                    "additionalProperties": false
                }
            }
        
    + Schema
            
            {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "model_schema": {
                        "$ref": "http://json-schema.org/draft-07/schema"
                    }
                },
                "required": ["name", "model_schema"],
                "additionalProperties": false
            }

+ Response 201

    + Headers
    
            Location: /api/cfg-cdns/media-library
        
## CfgCdn [/api/cfg-cdns/{name}]

+ Parameters

    + name (string, required) - The unique name of the CfgCdn
    
### Show a CfgCdn details [GET]

+ Response 200 (application/json)

    + Body

            {
                "id": "177a0f40-10e2-41df-98c8-cb75ebe990bc",
                "name": "media-library",
                "model_schema": {
                    "type": "object",
                    "additionalProperties": false
                }
            }
        
    + Schema
    
            {
                "$ref": "./json-schema/cfg-cdn.json"
            }

# Group CfgFilter

Resources related to the configuration models of filters.

## CfgFilter Collection [/api/cfg-filters]

### List all the CfgFilter [GET]

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": "addd4d8f-dd20-4031-a09b-c3e018e66b13",
                    "name": "thumbnail",
                    "mime_types": [
                        "image/jpeg",
                        "image/png"
                    ],
                    "model_schema": {
                        "type": "object",
                        "properties": {
                            "size": {
                                "type": "object",
                                "properties": {
                                    "width": {
                                        "type": "number"
                                    },
                                    "height": {
                                        "type": "number"
                                    }
                                },
                                "required": ["width", "height"],
                                "additionalProperties": false
                            },
                            "mode": {
                                "type": "string",
                                "enum": ["inset", "outbound"]
                            }
                        },
                        "required": ["size", "mode"],
                        "additionalProperties": false
                    }
                }
            ]
            
    + Schema
        
            {
                "$ref": "./json-schema/collection.cfg-filter.json"
            }

### Create a new CfgFilter [POST]

+ Request (application/json)

    + Body
    
            {
                "name": "thumbnail",
                "mime_types": [
                    "image/jpeg",
                    "image/png"
                ],
                "model_schema": {
                    "type": "object",
                    "properties": {
                        "size": {
                            "type": "object",
                            "properties": {
                                "width": {
                                    "type": "number"
                                },
                                "height": {
                                    "type": "number"
                                }
                            },
                            "required": ["width", "height"],
                            "additionalProperties": false
                        },
                        "mode": {
                            "type": "string",
                            "enum": ["inset", "outbound"]
                        }
                    },
                    "required": ["size", "mode"],
                    "additionalProperties": false
                }
            }
        
    + Schema
            
            {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "mime_types": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "model_schema": {
                        "$ref": "http://json-schema.org/draft-07/schema"
                    }
                },
                "required": ["name", "model_schema"],
                "additionalProperties": false
            }

+ Response 201

    + Headers
    
            Location: /api/cfg-filters/thumbnail
        
## CfgFilter [/api/cfg-filters/{name}]

+ Parameters

    + name (string, required) - The unique name of the CfgFilter
    
### Show a CfgFilter details [GET]

+ Response 200 (application/json)

    + Body

            {
                "id": "addd4d8f-dd20-4031-a09b-c3e018e66b13",
                "name": "thumbnail",
                "mime_types": [
                    "image/jpeg",
                    "image/png"
                ],
                "model_schema": {
                    "type": "object",
                    "properties": {
                        "size": {
                            "type": "object",
                            "properties": {
                                "width": {
                                    "type": "number"
                                },
                                "height": {
                                    "type": "number"
                                }
                            },
                            "required": ["width", "height"],
                            "additionalProperties": false
                        },
                        "mode": {
                            "type": "string",
                            "enum": ["inset", "outbound"]
                        }
                    },
                    "required": ["size", "mode"],
                    "additionalProperties": false
                }
            }
        
    + Schema
    
            {
                "$ref": "./json-schema/cfg-filter.json"
            }

# Group CfgMimeType

Resources related to the configuration models of mime types.

## CfgMimeType Collection [/api/cfg-mime-types]

### List all the CfgMimeType [GET]

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": "ac0b1a11-26c5-4c83-9370-3d4c7c88d89b",
                    "name": "image/jpeg"
                },
                {
                    "id": "30d6f1d2-fc21-44cc-92ce-9fee0dfef162",
                    "name": "image/png"
                }   
            ]
            
    + Schema
        
            {
                "$ref": "./json-schema/collection.cfg-mime-type.json"
            }

### Create a new CfgMimeType [POST]

+ Request (application/json)

    + Body
    
            {
                "name": "image/jpeg"
            }
        
    + Schema
            
            {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    }
                },
                "required": ["name"],
                "additionalProperties": false
            }

+ Response 201

    + Headers
    
            Location: /api/cfg-mime-type/ac0b1a11-26c5-4c83-9370-3d4c7c88d89b

## CfgMimeType [/api/cfg-mime-types/{id}]

+ Parameters

    + id (string, required) - The ID of the CfgMimeType
    
### Show a CfgMimeType details [GET]

+ Response 200 (application/json)

    + Body

            {
                "id": "ac0b1a11-26c5-4c83-9370-3d4c7c88d89b",
                "name": "image/jpeg"
            }
        
    + Schema
    
            {
                "$ref": "./json-schema/cfg-mime-type.json"
            }

# Group Cdn

Resources related to the configuration instances of CDNs.

## Cdn Collection [/api/workspaces/{workspace_id}/cdns]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace

### List all the Cdn [GET]

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": "fc05e27c-8bf9-4b66-b31f-0c87080476d5",
                    "cfg_cdn": "media-library",
                    "instance_values": {}
                }     
            ]
            
    + Schema
        
            {
                "$ref": "./json-schema/collection.cdn.json"
            }

### Create a new Cdn [POST]

+ Request (application/json)

    + Body
    
            {
                "cfg_cdn": "media-library",
                "instance_values": {}
            }
        
    + Schema
            
            {
                "type": "object",
                "properties": {
                    "cfg_cdn": {
                        "type": "string"
                    },
                    "instance_values": {}
                },
                "required": ["cfg_cdn", "instance_values"],
                "additionalProperties": false
            }

+ Response 201

    + Headers
    
            Location: /api/workspaces/cda13b90-6af0-4f7d-bd39-820c0257c743/cdns/fc05e27c-8bf9-4b66-b31f-0c87080476d5

## Cdn [/api/workspaces/{workspace_id}/cdns/{id}]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    + id (string, required) - The ID of the CDN

### Show a Cdn details [GET]

+ Response 200 (application/json)

    + Body

            {
                "cfg_cdn": "media-library",
                "instance_values": {}
            }
        
    + Schema
    
            {
                "$ref": "./json-schema/cdn.json"
            }

# Group Folder

Resources related to the managing of folders.

## Folder Collection [/api/workspaces/{workspace_id}/folders{?path}]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    + path (string, required) - The path of the target folder

### Search for a Folder [GET]

+ Response 200 (application/json)

    + Body

            {
                "id": "3120def7-a947-4a4a-9f63-990adcfedb73",
                "name": "thumbnails",
                "children": [],
                "media": [
                    "eae32719-a9ed-4c38-89e8-61f0597cc430"
                ]
            }
        
    + Schema
    
            {
                "$ref": "./json-schema/folder.json"
            }

### Create a new root Folder [POST]

+ Request (application/json)

    + Body
    
            {
                "name": "media"
            }
        
    + Schema
            
            {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    }
                },
                "required": ["name"],
                "additionalProperties": false
            }

+ Response 201

    + Headers
    
            Location: /api/workspaces/cda13b90-6af0-4f7d-bd39-820c0257c743/folders/ac803266-a12b-4a77-a033-f9b45fb1a6f0

## Folder [/api/workspaces/{workspace_id}/folders/{id}]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    + id (string, required) - The ID of the folder

### Show a Folder details [GET]

+ Response 200 (application/json)

    + Body

            {
                "id": "ac803266-a12b-4a77-a033-f9b45fb1a6f0",
                "name": "media",
                "children": [
                    {
                        "id": "3120def7-a947-4a4a-9f63-990adcfedb73",
                        "name": "thumbnails"
                    }
                ],
                "media": [
                    "2fa53b48-4e4b-4ec1-bee8-825f00b5c19e"
                ]
            }
        
    + Schema
    
            {
                "$ref": "./json-schema/folder.json"
            }

## Recursive Folder Collection [/api/workspaces/{workspace_id}/folders/{id}/folders]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    + id (string, required) - The ID of the folder
   
### Create a new nested Folder [POST]

+ Request (application/json)

    + Body
    
            {
                "name": "thumbnails"
            }
        
    + Schema
            
            {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    }
                },
                "required": ["name"],
                "additionalProperties": false
            }

+ Response 201

    + Headers
    
            Location: /api/workspaces/cda13b90-6af0-4f7d-bd39-820c0257c743/folders/3120def7-a947-4a4a-9f63-990adcfedb73

# Group Media

Resources related to the managing of media.

## Media Collection [/api/workspaces/{workspace_id}/media{?offset,limit}]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    + offset (number, required) - The offset to apply
    + limit (number, required) - The number of result to fetch
    
### List all the Media [GET]

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": "3c1354bb-db2c-4087-a1e0-7ac03271101b",
                    "cfg_mime_type": "image/jpeg",
                    "size": "32562",
                    "original_filename": "landscape",
                    "original_extension": "jpg",
                    "tags": ["landscape", "sunset", "beach"],
                    "description": "A sunset on the beach.",
                    "folder_id": null,
                    "transformations": [
                        {
                            "id": "64bc227f-9175-4106-b789-675086d88afa",
                            "name": "thumbnail"
                        }
                    ]
                }
            ]
            
    + Schema
        
            {
                "$ref": "./json-schema/collection.media.json"
            }
            
### Send a new Media [POST]

+ Request (multipart/form-data)

    + Attributes (object)
        + file (string, required) - The file BLOB
        + tags (array) - An array of tags for the sent media
            + tag (string)
        + description (string) - The description of the media
        + folder_id (string) - The ID of the folder where to place the media
        + transformations (array) - An array of transformations to apply to the sent media
            + transformation (string)

+ Response 201

    + Headers
        
            Location: /api/workspaces/cda13b90-6af0-4f7d-bd39-820c0257c743/media/3c1354bb-db2c-4087-a1e0-7ac03271101b
       
## Media [/api/workspaces/{workspace_id}/media/{id}]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    + id (string, required) - The ID of the media

### Show a Media details [GET]
    
+ Response 200 (application/json)
    
    + Body
    
            {
                "id": "3c1354bb-db2c-4087-a1e0-7ac03271101b",
                "cfg_mime_type": "image/jpeg",
                "size": "32562",
                "original_filename": "landscape",
                "original_extension": "jpg",
                "tags": ["landscape", "sunset", "beach"],
                "description": "A sunset on the beach.",
                "folder_id": null,
                "transformations": [
                    {
                        "id": "64bc227f-9175-4106-b789-675086d88afa",
                        "name": "thumbnail"
                    }
                ]
            }
            
    + Schema
        
            {
                "$ref": "./json-schema/media.json"
            }
            
## Transform Media [/api/workspaces/{workspace_id}/media/{id}/transform]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    + id (string, required) - The ID of the media
    
### Transform a Media [POST]

+ Request (application/json)

    + Body
    
            {
                "name": "thumbnail"
            }
    
    + Schema
    
            {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    }
                },
                "required": ["name"],
                "additionalProperties": false
            }

+ Response 204

# Group Transformation

Resources related to the managing of the transformations.

## Transformation Collection [/api/workspaces/{workspace_id}/transformations]

+ Parameters

    + workspace_id (string, required) - The ID of the workspace
    
### List all the Transformation [GET]

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": "7b274be4-6a57-45ca-b4a2-0af0bd685e15",
                    "name": "thumbnail",
                    "filters": [
                        {
                            "cfg_filter": "thumbnail",
                            "instance_values": {
                                "size": {
                                    "width": 100,
                                    "height": 100
                                },
                                "mode": "inset"
                            }
                        },
                        {
                            "cfg_filter": "grayscale",
                            "instance_values": {}
                        },
                        {
                            "cfg_filter": "optimize-png",
                            "instance_values": {
                                "compression_level": 7
                            }
                        }
                    ]
                }
            ]
            
    + Schema
        
            {
                "$ref": "./json-schema/collection.transformation.json"
            }

### Create a new Transformation [POST]

+ Request (application/json)

    + Body
    
            {
                "name": "thumbnail",
                "filters": [
                    {
                        "cfg_filter": "thumbnail",
                        "instance_values": {
                            "size": {
                                "width": 100,
                                "height": 100
                            },
                            "mode": "inset"
                        }
                    },
                    {
                        "cfg_filter": "grayscale",
                        "instance_values": {}
                    },
                    {
                        "cfg_filter": "optimize-png",
                        "instance_values": {
                            "compression_level": 7
                        }
                    }
                ]
            }
        
    + Schema
            
            {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "filters": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "cfg_filter": {
                                    "type": "string"s
                                },
                                "instance_values": {}
                            },
                            "required": ["cfg_filter", "instance_values"],
                            "additionalProperties": false
                        }
                    }
                },
                "required": ["name", "filters"],
                "additionalProperties": false
            }

+ Response 201

    + Headers
    
            Location: /api/workspaces/cda13b90-6af0-4f7d-bd39-820c0257c743/transformations/7b274be4-6a57-45ca-b4a2-0af0bd685e15
        
## Transformation [/api/workspaces/{workspace_id}/transformations/{name}]

+ Parameters
    
    + workspace_id (string, required) - The ID of the workspace
    + name (string, required) - The unique name of the Transformation in the workspace
    
### Show a Transformation details [GET]

+ Response 200 (application/json)

    + Body

            {
                "id": "7b274be4-6a57-45ca-b4a2-0af0bd685e15",
                "name": "thumbnail",
                "filters": [
                    {
                        "cfg_filter": "thumbnail",
                        "instance_values": {
                            "size": {
                                "width": 100,
                                "height": 100
                            },
                            "mode": "inset"
                        }
                    },
                    {
                        "cfg_filter": "grayscale",
                        "instance_values": {}
                    },
                    {
                        "cfg_filter": "optimize-png",
                        "instance_values": {
                            "compression_level": 7
                        }
                    }
                ]
            }
        
    + Schema
    
            {
                "$ref": "./json-schema/transformation.json"
            }

# Group TransformedMedia

Resources related to the managing of the transformed media.

## TransformedMedia [/api/workspaces/{workspace_id}/media/{media_id}/transformed-media]

+ Parameters
    
    + workspace_id (string, required) - The ID of the workspace
    + media_id (string, required) -  The ID of the media
    
### List all the TransformedMedia [GET]

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": "eea94d56-8e4e-4de6-9167-6e449b9604fe",
                    "size": 14631,
                    "transformation": "thumbnail",
                    "publications": [
                        {
                            "cfg_cdn": "media-library",
                            "public_url": "http://media-library.app.io/media/78/c5/7246-efe7-4046-8d8c-b9af062591cb.png
                        }
                    ]   
                }
            ]
            
    + Schema
        
            {
                "$ref": "./json-schema/collection.transformed-media.json"
            }
