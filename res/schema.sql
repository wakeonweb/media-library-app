CREATE TABLE folder (
  id uuid NOT NULL,
  workspace_id uuid NOT NULL,
  name varchar NOT NULL,
  parent_id uuid,

  PRIMARY KEY (id),
  UNIQUE (parent_id, name),
  FOREIGN KEY (parent_id) REFERENCES folder (id) ON DELETE CASCADE
);

CREATE TABLE cfg_mime_type (
  id uuid NOT NULL,
  name varchar NOT NULL UNIQUE,

  PRIMARY KEY (id)
);

CREATE TABLE cfg_filter (
  id uuid NOT NULL,
  name varchar NULL UNIQUE,
  model_schema jsonb NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE cfg_filter_mime_type (
  cfg_filter_id uuid NOT NULL,
  cfg_mime_type_id uuid NOT NULL,

  PRIMARY KEY (cfg_filter_id, cfg_mime_type_id),
  FOREIGN KEY (cfg_filter_id) REFERENCES cfg_filter (id) ON DELETE CASCADE,
  FOREIGN KEY (cfg_mime_type_id) REFERENCES cfg_mime_type (id) ON DELETE RESTRICT
);

CREATE TABLE media (
  id uuid NOT NULL,
  workspace_id uuid NOT NULL,
  cfg_mime_type_id uuid NOT NULL,
  size int8 NOT NULL,
  original_filename varchar NOT NULL,
  original_extension varchar NOT NULL,
  tags varchar[] NOT NULL,
  description text,
  folder_id uuid,

  PRIMARY KEY (id),
  FOREIGN KEY (cfg_mime_type_id) REFERENCES cfg_mime_type (id) ON DELETE RESTRICT,
  FOREIGN KEY (folder_id) REFERENCES folder (id) ON DELETE SET NULL
);

CREATE TABLE transformation (
  id uuid NOT NULL,
  workspace_id uuid NOT NULL,
  name varchar NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (workspace_id, name)
);

CREATE TABLE transformation_filter (
  transformation_id uuid NOT NULL,
  cfg_filter_id uuid NOT NULL,
  index int NOT NULL,
  instance_values jsonb NOT NULL,

  PRIMARY KEY (transformation_id, index),
  FOREIGN KEY (transformation_id) REFERENCES transformation (id) ON DELETE RESTRICT,
  FOREIGN KEY (cfg_filter_id) REFERENCES cfg_filter (id) ON DELETE CASCADE
);

CREATE TABLE media_transformation (
  id uuid NOT NULL,
  media_id uuid NOT NULL,
  transformation_id uuid NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (media_id, transformation_id),
  FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE,
  FOREIGN KEY (transformation_id) REFERENCES transformation (id) ON DELETE RESTRICT
);

CREATE TABLE transformed_media (
  id uuid NOT NULL,
  media_transformation_id uuid NOT NULL,
  extension varchar NOT NULL,
  size int8 NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (media_transformation_id) REFERENCES media_transformation (id) ON DELETE RESTRICT
);

CREATE TABLE cfg_cdn (
  id uuid NOT NULL,
  name varchar NOT NULL UNIQUE,
  model_schema jsonb NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE cdn (
  id uuid NOT NULL,
  workspace_id uuid NOT NULL,
  cfg_cdn_id uuid NOT NULL,
  instance_values jsonb NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (workspace_id, cfg_cdn_id),
  FOREIGN KEY (cfg_cdn_id) REFERENCES cfg_cdn (id) ON DELETE RESTRICT
);

CREATE TABLE published_transformed_media (
  transformed_media_id uuid NOT NULL,
  cdn_id uuid NOT NULL,
  public_url varchar NOT NULL,

  PRIMARY KEY (transformed_media_id, cdn_id),
  FOREIGN KEY (transformed_media_id) REFERENCES transformed_media (id) ON DELETE RESTRICT,
  FOREIGN KEY (cdn_id) REFERENCES cdn (id) ON DELETE RESTRICT
);
