Feature: Configure the mime types.

  As an administrator of the service
  In order to configure the service
  I need the ability to manipulate the configuration mime types

  Scenario: Create new configuration mime types.
     When I add "Content-Type" header equal to "application/json"
      And I send a POST request to "/api/cfg-mime-types" with body:
          """
          {
              "name": "image/jpeg"
          }
          """
     Then the response status code should be 201
      And the response should be empty

  @fixture:cfg_mime_type
  Scenario: Show a configuration mime type.
     When I send a GET request to "/api/cfg-mime-types/f9567c15-ea63-4f3b-b322-1b2eaa9b8e11"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/cfg-mime-type.json"

  @fixture:cfg_mime_type @fixture:cfg_filter
  Scenario: Show the list of configuration mime types.
     When I send a GET request to "/api/cfg-mime-types"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/collection.cfg-mime-type.json"

