Feature: Configure content delivery network.

  As an user of the service
  In order to publish some media on my content delivery network
  I need the ability to configure them

  @fixture:cfg_cdn
  Scenario: Configure new content delivery network.
     When I add "Content-Type" header equal to "application/json"
      And I send a POST request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/cdns" with body:
          """
          {
              "cfg_cdn": "media-library",
              "instance_values": {}
          }
          """
     Then the response status code should be 201
      And the response should be empty

  @fixture:cfg_cdn @fixture:cdn
  Scenario: Show a content delivery network configuration.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/cdns/f742c1dc-279a-4161-b696-9e7f61b4a1eb"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/cdn.json"

  @fixture:cfg_cdn @fixture:cdn
  Scenario: Show the list of content delivery network configurations.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/cdns"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/collection.cdn.json"
