Feature: Transform and publish media.

  As an user of the service
  In order to transform then publish some media
  I need the ability to execute those transformations

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:cfg_cdn @fixture:cdn @fixture:folder @fixture:transformation @fixture:media @fixture:transformed_media
  Scenario: Show the list of transformed media.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/media/be419bf6-b77a-4286-8f7f-4a04cfd3fe28/transformed-media"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/collection.transformed-media.json"

