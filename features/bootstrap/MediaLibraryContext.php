<?php

use Behat\Behat\Context\Context;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class MediaLibraryContext implements Context
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @beforeScenario
     * @afterScenario
     *
     * @throws DBALException
     */
    public function reset(): void
    {
        $this->emptyFolder(__DIR__.'/../../uploads/media');
        $this->emptyFolder(__DIR__.'/../../uploads/transformed-media');
        $this->emptyFolder(__DIR__.'/../../public/media');

        $this->connection->exec('TRUNCATE TABLE cdn CASCADE');
        $this->connection->exec('TRUNCATE TABLE cfg_cdn CASCADE');
        $this->connection->exec('TRUNCATE TABLE cfg_filter CASCADE');
        $this->connection->exec('TRUNCATE TABLE cfg_mime_type CASCADE');
        $this->connection->exec('TRUNCATE TABLE folder CASCADE');
        $this->connection->exec('TRUNCATE TABLE media CASCADE');
        $this->connection->exec('TRUNCATE TABLE transformation CASCADE');
        $this->connection->exec('TRUNCATE TABLE transformed_media CASCADE');
        $this->connection->exec('VACUUM');
    }

    /**
     * @beforeScenario @fixture:cfg_mime_type
     *
     * @throws DBALException
     */
    public function loadCfgMimeTypes(): void
    {
        foreach ($this->read(__DIR__ . '/../fixtures/cfg_mime_type.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO cfg_mime_type (id, name) VALUES (:id, :name)', $row);
        }
    }

    /**
     * @beforeScenario @fixture:cfg_filter
     *
     * @throws DBALException
     */
    public function loadCfgFilters(): void
    {
        foreach ($this->read(__DIR__ . '/../fixtures/cfg_filter.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO cfg_filter (id, name, model_schema) VALUES (:id, :name, :model_schema)', $row);
        }

        foreach ($this->read(__DIR__ . '/../fixtures/cfg_filter_mime_type.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO cfg_filter_mime_type (cfg_filter_id, cfg_mime_type_id) VALUES (:cfg_filter_id, :cfg_mime_type_id)', $row);
        }
    }

    /**
     * @beforeScenario @fixture:cfg_cdn
     *
     * @throws DBALException
     */
    public function loadCfgCdns(): void
    {
        foreach ($this->read(__DIR__ . '/../fixtures/cfg_cdn.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO cfg_cdn (id, name, model_schema) VALUES (:id, :name, :model_schema)', $row);
        }
    }

    /**
     * @beforeScenario @fixture:folder
     *
     * @throws DBALException
     */
    public function loadFolders(): void
    {
        foreach ($this->read(__DIR__ . '/../fixtures/folder.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO folder (id, workspace_id, name, parent_id) VALUES (:id, :workspace_id, :name, :parent_id)', $row);
        }
    }

    /**
     * @beforeScenario @fixture:transformation
     *
     * @throws DBALException
     */
    public function loadTransformations(): void
    {
        foreach ($this->read(__DIR__ . '/../fixtures/transformation.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO transformation (id, workspace_id, name) VALUES (:id, :workspace_id, :name)', $row);
        }

        foreach ($this->read(__DIR__ . '/../fixtures/transformation_filter.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO transformation_filter (transformation_id, cfg_filter_id, index, instance_values) VALUES (:transformation_id, :cfg_filter_id, :index, :instance_values)', $row);
        }
    }

    /**
     * @beforeScenario @fixture:media
     *
     * @throws DBALException
     */
    public function loadMedia(): void
    {
        $fs = new Filesystem();

        foreach ($this->read(__DIR__ . '/../fixtures/media.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO media (id, workspace_id, cfg_mime_type_id, "size", original_filename, original_extension, tags, description, folder_id) VALUES (:id, :workspace_id, :cfg_mime_type_id, :size, :original_filename, :original_extension, :tags, :description, :folder_id)', $row);
        }

        $fs->copy(
            __DIR__.'/../media/landscape.jpg',
            __DIR__.'/../../uploads/media/be/41/9bf6-b77a-4286-8f7f-4a04cfd3fe28.jpg'
        );

        $fs->copy(
            __DIR__.'/../media/portrait.jpg',
            __DIR__.'/../../uploads/media/8a/89/e2cc-0332-4a51-a68b-a8839d50f97b.jpg'
        );

        foreach ($this->read(__DIR__ . '/../fixtures/media_transformation.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO media_transformation (id, media_id, transformation_id) VALUES (:id, :media_id, :transformation_id)', $row);
        }
    }

    /**
     * @beforeScenario @fixture:cdn
     *
     * @throws DBALException
     */
    public function loadCdns(): void
    {
        foreach ($this->read(__DIR__ . '/../fixtures/cdn.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO cdn (id, cfg_cdn_id, workspace_id, instance_values) VALUES (:id, :cfg_cdn_id, :workspace_id, :instance_values)', $row);
        }
    }

    /**
     * @beforeScenario @fixture:transformed_media
     *
     * @throws DBALException
     */
    public function loadTransformedMedia(): void
    {
        $fs = new Filesystem();

        foreach ($this->read(__DIR__ . '/../fixtures/transformed_media.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO transformed_media (id, media_transformation_id, size, extension) VALUES (:id, :media_transformation_id, :size, :extension)', $row);
        }

        $fs->copy(
            __DIR__.'/../media/landscape.negative-black-and-white.jpg',
            __DIR__.'/../../uploads/transformed-media/a6/28/97b7-c791-41a1-acec-766813a4dfee.jpg'
        );

        foreach ($this->read(__DIR__ . '/../fixtures/published_transformed_media.csv') as $row) {
            $this->connection->executeQuery('INSERT INTO published_transformed_media (transformed_media_id, cdn_id, public_url) VALUES (:transformed_media_id, :cdn_id, :public_url)', $row);
        }

        $fs->copy(
            __DIR__.'/../../uploads/transformed-media/a6/28/97b7-c791-41a1-acec-766813a4dfee.jpg',
            __DIR__.'/../../public/media/a6/28/97b7-c791-41a1-acec-766813a4dfee.jpg'
        );
    }

    /**
     * @param string $path
     */
    private function emptyFolder(string $path): void
    {
        $fs = new Filesystem();

        foreach (new DirectoryIterator($path) as $value) {
            if ($value->isDot()) {
                continue;
            }

            if ($value->isDir()) {
                $fs->remove($value->getPathname());
            }
        }
    }

    /**
     * @param string $filename
     *
     * @return Generator
     */
    private function read(string $filename): Generator
    {
        $fp = fopen($filename, 'rb');

        $headers = fgetcsv($fp);

        while ($row = fgetcsv($fp)) {
            $row = array_map(
                function (string $value) {
                    return $value !== '' ? $value : null;
                },
                $row
            );

            yield array_combine($headers, $row);
        }

        fclose($fp);
    }
}
