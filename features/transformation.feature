Feature: Create transformations.

  As an user of the service
  In order to consume filters
  I need the ability to create and configure them

  @fixture:cfg_mime_type @fixture:cfg_filter
  Scenario: Create new transformation.
     When I add "Content-Type" header equal to "application/json"
      And I send a POST request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/transformations" with body:
          """
          {
              "name": "negative-black-and-white",
              "filters": [
                  {
                      "cfg_filter": "negative",
                      "instance_values": {}
                  },
                  {
                      "cfg_filter": "grayscale",
                      "instance_values": {}
                  },
                  {
                      "cfg_filter": "gamma",
                      "instance_values": {
                          "correction": 0.1
                      }
                  }
              ]
          }
          """
     Then the response status code should be 201
      And the response should be empty
      And the header "Location" should be equal to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/transformations/negative-black-and-white"

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:transformation
  Scenario: Show a transformation.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/transformations/negative-black-and-white"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/transformation.json"

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:transformation
  Scenario: Show the list of configuration transformations.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/transformations"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/collection.transformation.json"
