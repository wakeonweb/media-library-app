Feature: Configure the content delivery networks.

  As an administrator of the service
  In order to configure the service
  I need the ability to manipulate the configuration content delivery networks

  Scenario: Create new configuration content delivery networks.
     When I add "Content-Type" header equal to "application/json"
      And I send a POST request to "/api/cfg-cdns" with body:
          """
          {
              "name": "media-library",
              "model_schema": {
                  "type": "object",
                  "additionalProperties": false
              }
          }
          """
     Then the response status code should be 201
      And the response should be empty
      And the header "Location" should be equal to "/api/cfg-cdns/media-library"

  @fixture:cfg_cdn
  Scenario: Show a configuration content delivery network.
     When I send a GET request to "/api/cfg-cdns/media-library"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/cfg-cdn.json"

  @fixture:cfg_cdn
  Scenario: Show the list of configuration content delivery network.
     When I send a GET request to "/api/cfg-cdns"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/collection.cfg-cdn.json"
