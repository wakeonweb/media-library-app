Feature: Configure the filters.

  As an administrator of the service
  In order to configure the service
  I need the ability to manipulate the configuration filters

  @fixture:cfg_mime_type
  Scenario: Create new configuration filters.
     When I add "Content-Type" header equal to "application/json"
      And I send a POST request to "/api/cfg-filters" with body:
          """
          {
              "name": "resize",
              "mime_types": [
                  "image/jpeg",
                  "image/png"
              ],
              "model_schema": {
                  "type": "object",
                  "properties": {
                      "box": {
                          "type": "object",
                          "properties": {
                              "width": {
                                  "type": "number"
                              },
                              "height": {
                                  "type": "number"
                              }
                          },
                          "required": ["width", "height"]
                      }
                  },
                  "required": ["box"]
              }
          }
          """
     Then the response status code should be 201
      And the response should be empty
      And the header "Location" should be equal to "/api/cfg-filters/resize"

  @fixture:cfg_mime_type @fixture:cfg_filter
  Scenario: Show a configuration filter.
     When I send a GET request to "/api/cfg-filters/negative"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/cfg-filter.json"

  @fixture:cfg_mime_type @fixture:cfg_filter
  Scenario: Show the list of configuration filters.
     When I send a GET request to "/api/cfg-filters"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/collection.cfg-filter.json"
