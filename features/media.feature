Feature: Send media.

  As an user of the service
  In order to publish some media
  I need the ability to send them

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:folder @fixture:transformation @fixture:cfg_cdn @fixture:cdn
  Scenario: Send new medias.
     When I send a POST request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/media" with parameters:
          | key                | value                                |
          | file               | @media/landscape.jpg                 |
          | tags[0]            | horizon zero dawn                    |
          | tags[1]            | game                                 |
          | tags[2]            | ps4                                  |
          | description        | Aloy shooting a sawtooth.            |
          | folder_id          | 16e673a0-d394-47bd-b3de-cca00bd02e08 |
          | transformations[0] | negative-black-and-white             |
     Then the response status code should be 201
      And the response should be empty

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:folder @fixture:transformation @fixture:media
  Scenario: Show a media.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/media/be419bf6-b77a-4286-8f7f-4a04cfd3fe28"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/media.json"

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:folder @fixture:transformation @fixture:media
  Scenario: Show the list of media.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/media"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/collection.media.json"

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:folder @fixture:transformation @fixture:media
  Scenario: Apply a transformation on a media
     When I send a POST request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/media/8a89e2cc-0332-4a51-a68b-a8839d50f97b/transform" with body:
          """
          {
              "name": "negative-black-and-white"
          }
          """
     Then the response status code should be 204
      And the response should be empty
