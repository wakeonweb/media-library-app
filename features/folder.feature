Feature: Organize the medias.

  As an user of the service
  In order to organize the medias
  I need the ability to manipulate folders

  @fixture:folder
  Scenario: Create new root folders.
     When I add "Content-Type" header equal to "application/json"
      And I send a POST request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/folders" with body:
          """
          {
              "name": "videos"
          }
          """
     Then the response status code should be 201
      And the response should be empty

  @fixture:folder
  Scenario: Create new nested folders.
     When I add "Content-Type" header equal to "application/json"
      And I send a POST request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/folders/16e673a0-d394-47bd-b3de-cca00bd02e08/folders" with body:
          """
          {
              "name": "black-and-white"
          }
          """
     Then the response status code should be 201
      And the response should be empty

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:folder @fixture:transformation @fixture:media
  Scenario: Show a folder.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/folders/55bf7464-0978-4bef-8c85-00faea757ca1"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/folder.json"

  @fixture:cfg_mime_type @fixture:cfg_filter @fixture:folder @fixture:transformation @fixture:media
  Scenario: Show a folder using its path.
     When I send a GET request to "/api/workspaces/c870af43-a086-406b-b91e-84d64400f001/folders?path=images%2Fthumbnails"
     Then the response status code should be 200
      And the JSON should be valid according to the schema "res/json-schema/folder.json"
