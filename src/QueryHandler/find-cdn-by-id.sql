    SELECT jsonb_build_object(
             'id', c.id,
             'cfg_cdn', cc.name,
             'instance_values', c.instance_values
           )
      FROM cdn AS c

INNER JOIN cfg_cdn cc
        ON c.cfg_cdn_id = cc.id

     WHERE c.workspace_id = :workspace_id AND c.id = :id
