SELECT coalesce(jsonb_agg(a), cast('[]' AS jsonb))
  FROM
     (
          SELECT t.id,
                 t.name,
                 coalesce(jsonb_agg(jsonb_build_object('cfg_filter', cf.name, 'instance_values', tf.instance_values) ORDER BY tf.index ASC) FILTER (WHERE tf.transformation_id IS NOT NULL), cast('[]' AS jsonb)) AS filters
            FROM transformation AS t

       LEFT JOIN transformation_filter AS tf
              ON t.id = tf.transformation_id

       LEFT JOIN cfg_filter cf
              ON tf.cfg_filter_id = cf.id

           WHERE t.workspace_id = :workspace_id
        GROUP BY t.id,
                 t.name
        ORDER BY t.name ASC
     ) AS a
