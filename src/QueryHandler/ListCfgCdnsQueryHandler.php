<?php

namespace App\QueryHandler;

use App\Query\ListCfgCdns;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListCfgCdnsQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ListCfgCdns $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(ListCfgCdns $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/get-cfg-cdns.sql'));
        $statement->execute();

        return $statement->fetchColumn() ?: null;
    }
}
