<?php

namespace App\QueryHandler;

use App\Query\FindCfgFilterByName;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindCfgFilterByNameQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param FindCfgFilterByName $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(FindCfgFilterByName $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/find-cfg-filter-by-name.sql'));
        $statement->execute([
            'name' => $query->name(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
