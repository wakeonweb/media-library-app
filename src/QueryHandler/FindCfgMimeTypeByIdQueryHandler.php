<?php

namespace App\QueryHandler;

use App\Query\FindCfgMimeTypeById;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindCfgMimeTypeByIdQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param FindCfgMimeTypeById $query
     *
     * @return mixed
     *
     * @throws DBALException
     */
    public function __invoke(FindCfgMimeTypeById $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/find-cfg-mime-type-by-id.sql'));
        $statement->execute([
            'id' => (string) $query->id(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
