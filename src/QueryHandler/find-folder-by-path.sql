WITH RECURSIVE tree (id, name, parent_id, path) AS
             (
                   SELECT f.id,
                          f.name,
                          f.parent_id,
                          f.name
                     FROM folder AS f
                    WHERE f.parent_id IS NULL AND workspace_id = :workspace_id

                UNION ALL

                   SELECT f.id,
                          f.name,
                          f.parent_id,
                          format('%s/%s', t.path, f.name)
                     FROM folder AS f
               INNER JOIN tree AS t
                       ON f.parent_id = t.id
             )
        SELECT jsonb_build_object(
                 'id', t.id,
                 'name', t.name,
                 'children', coalesce(jsonb_agg(jsonb_build_object('id', cf.id, 'name', cf.name) ORDER BY cf.name ASC) FILTER (WHERE cf.id IS NOT NULL), cast('[]' AS jsonb)),
                 'media', m.ids
               )
          FROM tree AS t

     LEFT JOIN folder cf
            ON t.id = cf.parent_id

     LEFT JOIN LATERAL
             (
               SELECT coalesce(jsonb_agg(_m.id) FILTER (WHERE _m.id IS NOT NULL), cast('[]' AS jsonb)) AS ids
                 FROM media AS _m
                WHERE _m.folder_id = t.id
             ) AS m
            ON true

         WHERE t.path = :path
      GROUP BY t.id,
               t.name,
               m.ids
