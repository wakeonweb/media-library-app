    SELECT jsonb_build_object(
             'id', m.id,
             'cfg_mime_type', cmt.name,
             'size', m.size,
             'original_filename', m.original_filename,
             'original_extension', m.original_extension,
             'tags', m.tags,
             'description', m.description,
             'folder_id', m.folder_id,
             'transformations', coalesce(jsonb_agg(jsonb_build_object('id', t.id, 'name', t.name) ORDER BY t.name ASC) FILTER (WHERE t.id IS NOT NULL), cast('[]' AS jsonb))
           )
      FROM media AS m

INNER JOIN cfg_mime_type AS cmt
        ON m.cfg_mime_type_id = cmt.id

 LEFT JOIN media_transformation AS mt
        ON m.id = mt.media_id

 LEFT JOIN transformation AS t
        ON mt.transformation_id = t.id

     WHERE m.workspace_id = :workspace_id AND m.id = :id
  GROUP BY m.id,
           cmt.name,
           m.size,
           m.original_filename,
           m.original_extension,
           m.tags,
           m.description,
           m.folder_id
