SELECT jsonb_build_object(
         'id', mt.id,
         'name', mt.name
       )
  FROM cfg_mime_type AS mt
 WHERE mt.id = :id
