SELECT coalesce(jsonb_agg(a), cast('[]' AS jsonb))
  FROM
     (
           SELECT m.id,
                  cmt.name AS cfg_mime_type,
                  m.size,
                  m.original_filename,
                  m.original_extension,
                  m.tags,
                  m.description,
                  m.folder_id,
                  coalesce(jsonb_agg(jsonb_build_object('id', t.id, 'name', t.name) ORDER BY t.name ASC) FILTER (WHERE t.id IS NOT NULL), cast('[]' AS jsonb)) AS transformations
             FROM media AS m

       INNER JOIN cfg_mime_type AS cmt
               ON m.cfg_mime_type_id = cmt.id

        LEFT JOIN media_transformation AS mt
               ON m.id = mt.media_id

        LEFT JOIN transformation AS t
               ON mt.transformation_id = t.id

            WHERE m.workspace_id = :workspace_id
         GROUP BY m.id,
                  cmt.name,
                  m.size,
                  m.original_filename,
                  m.original_extension,
                  m.tags,
                  m.description,
                  m.folder_id
         ORDER BY m.original_filename ASC
            LIMIT :limit
           OFFSET :offset
     ) AS a
