<?php

namespace App\QueryHandler;

use App\Query\ListTransformations;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListTransformationsQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ListTransformations $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(ListTransformations $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/get-transformations.sql'));
        $statement->execute([
            'workspace_id' => (string) $query->workspaceId(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
