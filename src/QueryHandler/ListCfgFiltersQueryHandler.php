<?php

namespace App\QueryHandler;

use App\Query\ListCfgFilters;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListCfgFiltersQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ListCfgFilters $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(ListCfgFilters $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/get-cfg-filters.sql'));
        $statement->execute();

        return $statement->fetchColumn() ?: null;
    }
}
