<?php

namespace App\QueryHandler;

use App\Query\ListTransformedMedia;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListTransformedMediaQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ListTransformedMedia $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(ListTransformedMedia $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/get-transformed-media.sql'));
        $statement->execute([
            'workspace_id' => (string) $query->workspaceId(),
            'media_id' => (string) $query->mediaId(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
