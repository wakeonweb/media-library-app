SELECT coalesce(jsonb_agg(a), cast('[]' AS jsonb))
  FROM
     (
           SELECT tm.id,
                  tm.size,
                  t.name AS transformation,
                  coalesce(jsonb_agg(jsonb_build_object('public_url', ptm.public_url, 'cfg_cdn', cc.name)) FILTER (WHERE ptm.transformed_media_id IS NOT NULL), cast('[]' AS jsonb)) AS publications
             FROM transformed_media AS tm

        LEFT JOIN published_transformed_media AS ptm
               ON ptm.transformed_media_id = tm.id

        LEFT JOIN cdn AS c
               ON c.id = ptm.cdn_id

        LEFT JOIN cfg_cdn AS cc
               ON cc.id = c.cfg_cdn_id

       INNER JOIN media_transformation AS mt
               ON tm.media_transformation_id = mt.id

       INNER JOIN transformation AS t
               ON t.id = mt.transformation_id

       INNER JOIN media AS m
               ON mt.media_id = m.id

            WHERE m.workspace_id = :workspace_id AND m.id = :media_id
         GROUP BY tm.id,
                  tm.size,
                  t.name
         ORDER BY t.name ASC
     ) AS a
