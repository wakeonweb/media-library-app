<?php

namespace App\QueryHandler;

use App\Query\ListCfgMimeTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListCfgMimeTypesQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ListCfgMimeTypes $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(ListCfgMimeTypes $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/get-cfg-mime-types.sql'));
        $statement->execute();

        return $statement->fetchColumn() ?: null;
    }
}
