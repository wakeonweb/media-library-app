<?php

namespace App\QueryHandler;

use App\Query\FindFolderById;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindFolderByIdQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param FindFolderById $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(FindFolderById $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/find-folder-by-id.sql'));
        $statement->execute([
            'workspace_id' => (string) $query->workspaceId(),
            'id' => (string) $query->id(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
