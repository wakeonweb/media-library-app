   SELECT jsonb_build_object(
            'id', t.id,
            'name', t.name,
            'filters', coalesce(jsonb_agg(jsonb_build_object('cfg_filter', cf.name, 'instance_values', tf.instance_values) ORDER BY tf.index ASC) FILTER (WHERE tf.transformation_id IS NOT NULL), cast('[]' AS jsonb))
          )
     FROM transformation AS t

LEFT JOIN transformation_filter AS tf
       ON t.id = tf.transformation_id

LEFT JOIN cfg_filter cf
       ON tf.cfg_filter_id = cf.id

    WHERE (t.workspace_id, t.name) = (:workspace_id, :name)
 GROUP BY t.id,
          t.name

