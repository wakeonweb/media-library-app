SELECT coalesce(jsonb_agg(a), cast('[]' AS jsonb))
  FROM
     (
           SELECT c.id,
                  cc.name AS cfg_cdn,
                  c.instance_values
             FROM cdn AS c

       INNER JOIN cfg_cdn cc
               ON c.cfg_cdn_id = cc.id

         ORDER BY c.id ASC
     ) AS a
