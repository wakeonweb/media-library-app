<?php

namespace App\QueryHandler;

use App\Query\FindCfgCdnByName;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindCfgCdnByNameQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param FindCfgCdnByName $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(FindCfgCdnByName $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/find-cfg-cdn-by-name.sql'));
        $statement->execute([
            'name' => $query->name(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
