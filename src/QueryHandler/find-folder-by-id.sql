   SELECT jsonb_build_object(
            'id', f.id,
            'name', f.name,
            'children', coalesce(jsonb_agg(jsonb_build_object('id', cf.id, 'name', cf.name) ORDER BY cf.name ASC) FILTER (WHERE cf.id IS NOT NULL), cast('[]' AS jsonb)),
            'media', m.ids
          )
     FROM folder AS f

LEFT JOIN folder cf
       ON f.id = cf.parent_id

LEFT JOIN LATERAL
        (
          SELECT coalesce(jsonb_agg(_m.id) FILTER (WHERE _m.id IS NOT NULL), cast('[]' AS jsonb)) AS ids
            FROM media AS _m
           WHERE _m.folder_id = f.id
        ) AS m
       ON true

    WHERE f.workspace_id = :workspace_id AND f.id = :id
 GROUP BY f.id,
          f.name,
          m.ids
