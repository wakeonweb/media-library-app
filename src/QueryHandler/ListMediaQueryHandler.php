<?php

namespace App\QueryHandler;

use App\Query\ListMedia;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListMediaQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ListMedia $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(ListMedia $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/get-media.sql'));
        $statement->execute([
            'workspace_id' => (string) $query->workspaceId(),
            'offset' => $query->offset(),
            'limit' => $query->limit()
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
