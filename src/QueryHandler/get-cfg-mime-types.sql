SELECT coalesce(jsonb_agg(a), cast('[]' AS jsonb))
  FROM
     (
         SELECT mt.id,
                mt.name
           FROM cfg_mime_type AS mt
       ORDER BY mt.name ASC
     ) AS a
