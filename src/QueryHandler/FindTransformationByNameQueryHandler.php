<?php

namespace App\QueryHandler;

use App\Query\FindTransformationByName;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindTransformationByNameQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param FindTransformationByName $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(FindTransformationByName $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/find-transformation-by-name.sql'));
        $statement->execute([
            'workspace_id' => (string) $query->workspaceId(),
            'name' => $query->name(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
