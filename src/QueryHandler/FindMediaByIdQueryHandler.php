<?php

namespace App\QueryHandler;

use App\Query\FindMediaById;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindMediaByIdQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param FindMediaById $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(FindMediaById $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/find-media-by-id.sql'));
        $statement->execute([
            'workspace_id' => (string) $query->workspaceId(),
            'id' => (string) $query->id(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
