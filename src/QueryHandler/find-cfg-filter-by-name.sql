   SELECT jsonb_build_object(
            'id', f.id,
            'name', f.name,
            'model_schema', f.model_schema,
            'mime_types', coalesce(jsonb_agg(mt.name) FILTER (WHERE mt IS NOT NULL), cast('[]' AS jsonb))
          )
     FROM cfg_filter AS f

LEFT JOIN cfg_filter_mime_type AS fmt
       ON fmt.cfg_filter_id = f.id

LEFT JOIN cfg_mime_type AS mt
       ON mt.id = fmt.cfg_mime_type_id

    WHERE f.name = :name
 GROUP BY f.id,
          f.name,
          f.model_schema
