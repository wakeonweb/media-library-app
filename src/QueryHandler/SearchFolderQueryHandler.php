<?php

namespace App\QueryHandler;

use App\Query\SearchFolder;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class SearchFolderQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param SearchFolder $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(SearchFolder $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__ . '/find-folder-by-path.sql'));
        $statement->execute([
            'workspace_id' => (string) $query->workspaceId(),
            'path' => $query->path(),
        ]);

        return $statement->fetchColumn() ?: null;
    }
}
