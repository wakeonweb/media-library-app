SELECT jsonb_build_object(
         'id', cc.id,
         'name', cc.name,
         'model_schema', cc.model_schema
       )
  FROM cfg_cdn AS cc
 WHERE cc.name = :name
