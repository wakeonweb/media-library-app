SELECT coalesce(jsonb_agg(a), cast('[]' AS jsonb))
  FROM
     (
         SELECT cc.id,
                cc.name,
                cc.model_schema
           FROM cfg_cdn AS cc
       ORDER BY cc.name ASC
     ) AS a
