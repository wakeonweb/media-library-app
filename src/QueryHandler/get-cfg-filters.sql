SELECT coalesce(jsonb_agg(a), cast('[]' AS jsonb))
  FROM
     (
          SELECT f.id,
                 f.name,
                 f.model_schema,
                 coalesce(jsonb_agg(mt.name) FILTER (WHERE mt IS NOT NULL), cast('[]' AS jsonb)) AS mime_types
            FROM cfg_filter AS f

       LEFT JOIN cfg_filter_mime_type AS fmt
              ON fmt.cfg_filter_id = f.id

       LEFT JOIN cfg_mime_type AS mt
              ON mt.id = fmt.cfg_mime_type_id

        GROUP BY f.id,
                 f.name,
                 f.model_schema
        ORDER BY f.name ASC
     ) AS a
