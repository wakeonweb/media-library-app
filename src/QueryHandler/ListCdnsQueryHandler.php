<?php

namespace App\QueryHandler;

use App\Query\ListCdns;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListCdnsQueryHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ListCdns $query
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function __invoke(ListCdns $query): ?string
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/get-cdns.sql'));
        $statement->execute();

        return $statement->fetchColumn() ?: null;
    }
}
