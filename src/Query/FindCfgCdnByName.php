<?php

namespace App\Query;

use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindCfgCdnByName
{
    /**
     * @param Request $request
     *
     * @return FindCfgCdnByName
     */
    public static function fromRequest(Request $request): self
    {
        return new self($request->attributes->get('name'));
    }

    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
