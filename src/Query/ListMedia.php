<?php

namespace App\Query;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListMedia
{
    /**
     * @param Request $request
     *
     * @return ListMedia
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            Uuid::fromString($request->attributes->get('workspace_id')),
            $request->query->getInt('offset', 0),
            $request->query->getInt('limit', 25)
        );
    }

    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $limit;

    /**
     * @param UuidInterface $workspaceId
     * @param int           $offset
     * @param int           $limit
     */
    public function __construct(UuidInterface $workspaceId, int $offset = 0, int $limit = 25)
    {
        $this->workspaceId = $workspaceId;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return int
     */
    public function offset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }
}
