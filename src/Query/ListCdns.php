<?php

namespace App\Query;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListCdns
{
    /**
     * @param Request $request
     *
     * @return ListCdns
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            Uuid::fromString($request->attributes->get('workspace_id'))
        );
    }

    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @param UuidInterface $workspaceId
     */
    public function __construct(UuidInterface $workspaceId)
    {
        $this->workspaceId = $workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }
}
