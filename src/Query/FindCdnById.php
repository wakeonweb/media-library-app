<?php

namespace App\Query;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindCdnById
{
    /**
     * @param Request $request
     *
     * @return FindCdnById
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            Uuid::fromString($request->attributes->get('workspace_id')),
            Uuid::fromString($request->attributes->get('id'))
        );
    }

    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @param UuidInterface $workspaceId
     * @param UuidInterface $id
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $id)
    {
        $this->workspaceId = $workspaceId;
        $this->id = $id;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }
}
