<?php

namespace App\Query;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindTransformationByName
{
    /**
     * @param Request $request
     *
     * @return FindTransformationByName
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            Uuid::fromString($request->attributes->get('workspace_id')),
            $request->attributes->get('name')
        );
    }

    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var string
     */
    private $name;

    /**
     * @param UuidInterface $workspaceId
     * @param string        $name
     */
    public function __construct(UuidInterface $workspaceId, string $name)
    {
        $this->workspaceId = $workspaceId;
        $this->name = $name;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
