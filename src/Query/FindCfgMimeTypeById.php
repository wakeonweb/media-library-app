<?php

namespace App\Query;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FindCfgMimeTypeById
{
    /**
     * @param Request $request
     *
     * @return FindCfgMimeTypeById
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            Uuid::fromString($request->attributes->get('id'))
        );
    }

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @param UuidInterface $id
     */
    public function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }
}
