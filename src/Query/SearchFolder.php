<?php

namespace App\Query;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class SearchFolder
{
    /**
     * @param Request $request
     *
     * @return SearchFolder
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            Uuid::fromString($request->attributes->get('workspace_id')),
            $request->query->get('path')
        );
    }

    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var string
     */
    private $path;

    /**
     * @param UuidInterface $workspaceId
     * @param string        $path
     */
    public function __construct(UuidInterface $workspaceId, string $path)
    {
        $this->workspaceId = $workspaceId;
        $this->path = $path;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return string
     */
    public function path(): string
    {
        return $this->path;
    }
}
