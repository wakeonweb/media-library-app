<?php

namespace App\Query;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ListTransformedMedia
{
    /**
     * @param Request $request
     *
     * @return ListTransformedMedia
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            Uuid::fromString($request->attributes->get('workspace_id')),
            Uuid::fromString($request->attributes->get('media_id'))
        );
    }

    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $mediaId;

    /**
     * @param UuidInterface $workspaceId
     * @param UuidInterface $mediaId
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $mediaId)
    {
        $this->workspaceId = $workspaceId;
        $this->mediaId = $mediaId;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function mediaId(): UuidInterface
    {
        return $this->mediaId;
    }
}
