<?php

namespace App\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class TagArrayType extends Type
{
    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return sprintf('{%s}', implode(',', array_map(
            function (string $value): string {
                return $value === null ? 'NULL' : sprintf('"%s"', addcslashes($value, '"\\'));
            },
            $value
        )));
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        dump($value);
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'tag[]';
    }
}
