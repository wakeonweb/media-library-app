<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgMimeType
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @param UuidInterface $id
     * @param string        $name
     */
    public function __construct(UuidInterface $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
