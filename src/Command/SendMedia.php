<?php

namespace App\Command;

use App\Command\DTO\MediaTransformation;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class SendMedia
{
    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var string[]
     */
    private $tags;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var UuidInterface|null
     */
    private $folderId;

    /**
     * @var MediaTransformation[]
     */
    private $transformations;

    /**
     * @param UuidInterface         $workspaceId
     * @param UuidInterface         $id
     * @param UploadedFile          $file
     * @param string[]              $tags
     * @param string|null           $description
     * @param UuidInterface|null    $folderId
     * @param MediaTransformation[] $transformations
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $id, UploadedFile $file, array $tags, ?string $description, ?UuidInterface $folderId, array $transformations)
    {
        $this->workspaceId = $workspaceId;
        $this->id = $id;
        $this->file = $file;
        $this->tags = $tags;
        $this->description = $description;
        $this->folderId = $folderId;
        $this->transformations = $transformations;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return UploadedFile
     */
    public function file(): UploadedFile
    {
        return $this->file;
    }

    /**
     * @return string[]
     */
    public function tags(): array
    {
        return $this->tags;
    }

    /**
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return UuidInterface|null
     */
    public function folderId(): ?UuidInterface
    {
        return $this->folderId;
    }

    /**
     * @return MediaTransformation[]
     */
    public function transformations(): array
    {
        return $this->transformations;
    }

    /**
     * @return bool
     */
    public function isPlacedInFolder(): bool
    {
        return $this->folderId !== null;
    }
}
