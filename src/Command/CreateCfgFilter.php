<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgFilter
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string[]
     */
    private $mimeTypes;

    /**
     * @var object
     */
    private $modelSchema;

    /**
     * @param UuidInterface $id
     * @param string        $name
     * @param string[]      $mimeTypes
     * @param object        $modelSchema
     */
    public function __construct(UuidInterface $id, string $name, array $mimeTypes, object $modelSchema)
    {
        $this->id = $id;
        $this->name = $name;
        $this->mimeTypes = $mimeTypes;
        $this->modelSchema = $modelSchema;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function mimeTypes(): array
    {
        return $this->mimeTypes;
    }

    /**
     * @return object
     */
    public function modelSchema(): object
    {
        return $this->modelSchema;
    }
}
