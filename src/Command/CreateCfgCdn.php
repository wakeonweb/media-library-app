<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgCdn
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var object
     */
    private $modelSchema;

    /**
     * @param UuidInterface $id
     * @param string        $name
     * @param object        $modelSchema
     */
    public function __construct(UuidInterface $id, string $name, object $modelSchema)
    {
        $this->id = $id;
        $this->name = $name;
        $this->modelSchema = $modelSchema;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return object
     */
    public function modelSchema(): object
    {
        return $this->modelSchema;
    }
}
