<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class TransformMedia
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var UuidInterface
     */
    private $mediaTransformationId;

    /**
     * @param UuidInterface $id
     * @param UuidInterface $mediaTransformationId
     */
    public function __construct(UuidInterface $id, UuidInterface $mediaTransformationId)
    {
        $this->id = $id;
        $this->mediaTransformationId = $mediaTransformationId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return UuidInterface
     */
    public function mediaTransformationId(): UuidInterface
    {
        return $this->mediaTransformationId;
    }
}
