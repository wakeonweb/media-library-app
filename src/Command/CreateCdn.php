<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCdn
{
    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $cfgCdn;

    /**
     * @var object
     */
    private $instanceValues;

    /**
     * @param UuidInterface $workspaceId
     * @param UuidInterface $id
     * @param string        $cfgCdn
     * @param object        $instanceValues
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $id, string $cfgCdn, object $instanceValues)
    {
        $this->workspaceId = $workspaceId;
        $this->id = $id;
        $this->cfgCdn = $cfgCdn;
        $this->instanceValues = $instanceValues;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function cfgCdn(): string
    {
        return $this->cfgCdn;
    }

    /**
     * @return object
     */
    public function instanceValues(): object
    {
        return $this->instanceValues;
    }
}
