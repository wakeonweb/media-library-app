<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class PublishTransformedMedia
{
    /**
     * @var UuidInterface
     */
    private $cdnId;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @param UuidInterface $cdnId
     * @param UuidInterface $id
     */
    public function __construct(UuidInterface $cdnId, UuidInterface $id)
    {
        $this->cdnId = $cdnId;
        $this->id = $id;
    }

    /**
     * @return UuidInterface
     */
    public function cdnId(): UuidInterface
    {
        return $this->cdnId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }
}
