<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class AddMediaTransformation
{
    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $mediaId;

    /**
     * @var string
     */
    private $transformation;

    /**
     * @param UuidInterface $workspaceId
     * @param UuidInterface $mediaId
     * @param string        $transformation
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $mediaId, string $transformation)
    {
        $this->workspaceId = $workspaceId;
        $this->mediaId = $mediaId;
        $this->transformation = $transformation;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function mediaId(): UuidInterface
    {
        return $this->mediaId;
    }

    /**
     * @return string
     */
    public function transformation(): string
    {
        return $this->transformation;
    }
}
