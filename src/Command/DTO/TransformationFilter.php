<?php

namespace App\Command\DTO;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class TransformationFilter
{
    /**
     * @var string
     */
    private $cfgFilter;

    /**
     * @var object
     */
    private $instanceValues;

    /**
     * @param string $cfgFilter
     * @param object $instanceValues
     */
    public function __construct(string $cfgFilter, object $instanceValues)
    {
        $this->cfgFilter = $cfgFilter;
        $this->instanceValues = $instanceValues;
    }

    /**
     * @return string
     */
    public function cfgFilter(): string
    {
        return $this->cfgFilter;
    }

    /**
     * @return object
     */
    public function instanceValues(): object
    {
        return $this->instanceValues;
    }
}
