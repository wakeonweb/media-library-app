<?php

namespace App\Command\DTO;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class MediaTransformation
{
    /**
     * @var string
     */
    private $transformation;

    /**
     * @param string $transformation
     */
    public function __construct(string $transformation)
    {
        $this->transformation = $transformation;
    }

    /**
     * @return string
     */
    public function transformation(): string
    {
        return $this->transformation;
    }
}
