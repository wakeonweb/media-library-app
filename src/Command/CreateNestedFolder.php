<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateNestedFolder
{
    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $parentId;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @param UuidInterface $workspaceId
     * @param UuidInterface $parentId
     * @param UuidInterface $id
     * @param string        $name
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $parentId, UuidInterface $id, string $name)
    {
        $this->workspaceId = $workspaceId;
        $this->parentId = $parentId;
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function parentId(): UuidInterface
    {
        return $this->parentId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
