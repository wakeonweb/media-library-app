<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateRootFolder
{
    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @param UuidInterface $id
     * @param UuidInterface $workspaceId
     * @param string        $name
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $id, string $name)
    {
        $this->workspaceId = $workspaceId;
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
