<?php

namespace App\Command;

use App\Command\DTO\TransformationFilter;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateTransformation
{
    /**
     * @var UuidInterface
     */
    private $workspaceId;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var TransformationFilter[]
     */
    private $filters;

    /**
     * @param UuidInterface          $workspaceId
     * @param UuidInterface          $id
     * @param string                 $name
     * @param TransformationFilter[] $filters
     */
    public function __construct(UuidInterface $workspaceId, UuidInterface $id, string $name, array $filters)
    {
        $this->workspaceId = $workspaceId;
        $this->id = $id;
        $this->name = $name;
        $this->filters = $filters;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return TransformationFilter[]
     */
    public function filters(): array
    {
        return $this->filters;
    }
}
