<?php

namespace App\Publication;

use App\Entity\Cdn\Cdn;
use App\Entity\TransformedMedia\TransformedMedia;
use App\Utils\PathUtils;
use Psr\Container\ContainerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Publisher
{
    /**
     * @var ContainerInterface
     */
    private $publishers;

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @param ContainerInterface $publishers
     * @param string             $uploadDir
     */
    public function __construct(ContainerInterface $publishers, string $uploadDir)
    {
        $this->publishers = $publishers;
        $this->uploadDir = $uploadDir;
    }

    /**
     * @param TransformedMedia $transformedMedia
     * @param Cdn              $cdn
     *
     * @return string
     */
    public function publish(TransformedMedia $transformedMedia, Cdn $cdn): string
    {
        /** @var CdnPublisher $publisher */
        $publisher = $this->publishers->get((string) $cdn->cfgCdn());

        $media = $transformedMedia->mediaTransformation()->media();

        $path = PathUtils::uuidToPath((string) $transformedMedia->id(), sprintf('%s/transformed-media', $this->uploadDir), $media->originalExtension());

        return $publisher->publish($path, $cdn->instanceValues());
    }
}
