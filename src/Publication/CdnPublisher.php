<?php

namespace App\Publication;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
interface CdnPublisher
{
    /**
     * @param string $path
     * @param object $values
     *
     * @return string
     */
    public function publish(string $path, object $values): string;
}
