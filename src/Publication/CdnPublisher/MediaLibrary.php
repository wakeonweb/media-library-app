<?php

namespace App\Publication\CdnPublisher;

use App\Publication\CdnPublisher;
use App\Utils\PathUtils;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class MediaLibrary implements CdnPublisher
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @param Filesystem            $filesystem
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(Filesystem $filesystem, UrlGeneratorInterface $urlGenerator)
    {
        $this->filesystem = $filesystem;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function publish(string $path, object $values): string
    {
        $this->filesystem->copy(
            $path,
            $filename = PathUtils::pathToPublic($path)
        );

        return $this->urlGenerator->generate('cdn.media_library', ['filename' => $filename], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
