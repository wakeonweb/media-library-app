<?php

namespace App\Entity\Media;

use App\Entity\Transformation\Transformation;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\Media\MediaTransformationRepository")
 * @ORM\Table(name="media_transformation", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"media_id", "transformation_id"})
 * })
 */
class MediaTransformation
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media\Media", inversedBy="mediaTransformations")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=false)
     */
    private $media;

    /**
     * @var Transformation
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Transformation\Transformation")
     * @ORM\JoinColumn(name="transformation_id", referencedColumnName="id", nullable=false)
     */
    private $transformation;

    /**
     * @param UuidInterface  $id
     * @param Media          $media
     * @param Transformation $transformation
     */
    public function __construct(UuidInterface $id, Media $media, Transformation $transformation)
    {
        $this->id = $id;
        $this->media = $media;
        $this->transformation = $transformation;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return Media
     */
    public function media(): Media
    {
        return $this->media;
    }

    /**
     * @return Transformation
     */
    public function transformation(): Transformation
    {
        return $this->transformation;
    }
}
