<?php

namespace App\Entity\Media;

use App\Entity\CfgMimeType\CfgMimeType;
use App\Entity\Folder\Folder;
use App\Entity\Transformation\Transformation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\Media\MediaRepository")
 * @ORM\Table(name="media")
 */
class Media
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var UuidInterface
     *
     * @ORM\Column(name="workspace_id", type="guid", nullable=false)
     */
    private $workspaceId;

    /**
     * @var CfgMimeType
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CfgMimeType\CfgMimeType")
     * @ORM\JoinColumn(name="cfg_mime_type_id", referencedColumnName="id", nullable=false)
     */
    private $cfgMimeType;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="original_filename", type="string", nullable=false)
     */
    private $originalFilename;

    /**
     * @var string
     *
     * @ORM\Column(name="original_extension", type="string", nullable=false)
     */
    private $originalExtension;

    /**
     * @var string[]
     *
     * @ORM\Column(name="tags", type="tag[]", nullable=false)
     */
    private $tags;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var Folder|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Folder\Folder")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id", nullable=true)
     */
    private $folder;

    /**
     * @var MediaTransformation[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Media\MediaTransformation", cascade={"ALL"}, orphanRemoval=true, mappedBy="media")
     */
    private $mediaTransformations;

    /**
     * @param UuidInterface $id
     * @param UuidInterface $workspaceId
     * @param CfgMimeType   $cfgMimeType
     * @param int           $size
     * @param string        $originalFilename
     * @param string        $originalExtension
     * @param string[]      $tags
     * @param string|null   $description
     * @param Folder|null   $folder
     */
    public function __construct(UuidInterface $id, UuidInterface $workspaceId, CfgMimeType $cfgMimeType, int $size, string $originalFilename, string $originalExtension, array $tags, ?string $description, ?Folder $folder)
    {
        $this->id = $id;
        $this->workspaceId = $workspaceId;
        $this->cfgMimeType = $cfgMimeType;
        $this->size = $size;
        $this->originalFilename = $originalFilename;
        $this->originalExtension = $originalExtension;
        $this->tags = $tags;
        $this->description = $description;
        $this->folder = $folder;
        $this->mediaTransformations = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return Uuid::fromString($this->workspaceId);
    }

    /**
     * @return CfgMimeType
     */
    public function cfgMimeType(): CfgMimeType
    {
        return $this->cfgMimeType;
    }

    /**
     * @return int
     */
    public function size(): int
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function originalFilename(): string
    {
        return $this->originalFilename;
    }

    /**
     * @return string
     */
    public function originalExtension(): string
    {
        return $this->originalExtension;
    }

    /**
     * @return string[]
     */
    public function tags(): array
    {
        return $this->tags;
    }

    /**
     * @return null|string
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return Folder|null
     */
    public function folder(): ?Folder
    {
        return $this->folder;
    }

    /**
     * @return MediaTransformation[]
     */
    public function mediaTransformations(): array
    {
        return $this->mediaTransformations->toArray();
    }

    /**
     * @param Transformation $transformation
     *
     * @return UuidInterface
     *
     * @throws Exception
     */
    public function addTransformation(Transformation $transformation): UuidInterface
    {
        $this->mediaTransformations->add($mediaTransformation = new MediaTransformation(Uuid::uuid4(), $this, $transformation));

        return $mediaTransformation->id();
    }
}
