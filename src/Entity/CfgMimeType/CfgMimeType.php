<?php

namespace App\Entity\CfgMimeType;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\CfgMimeType\CfgMimeTypeRepository")
 * @ORM\Table(name="cfg_mime_type")
 */
class CfgMimeType
{
    public const TYPE_AUDIO = 'audio';
    public const TYPE_IMAGE = 'image';
    public const TYPE_TEXT = 'text';
    public const TYPE_VIDEO = 'video';

    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    private $name;

    /**
     * @param UuidInterface $id
     * @param string        $name
     */
    public function __construct(UuidInterface $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function guessType(): ?string
    {
        switch (explode('/', $this->name)[0]) {
            case 'audio':
                return self::TYPE_AUDIO;

            case 'image':
                return self::TYPE_IMAGE;

            case 'text':
                return self::TYPE_TEXT;

            case 'video':
                return self::TYPE_VIDEO;

            default:
                return null;
        }
    }

    /**
     * @param string $mimeType
     *
     * @return bool
     */
    public function equalsTo(string $mimeType): bool
    {
        return $this->name === $mimeType;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
