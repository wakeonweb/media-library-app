<?php

namespace App\Entity\Transformation;

use App\Entity\CfgFilter\CfgFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="transformation_filter")
 */
class TransformationFilter
{
    /**
     * @var Transformation
     *
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Transformation\Transformation", inversedBy="filters")
     * @ORM\JoinColumn(name="transformation_id", referencedColumnName="id", nullable=false)
     */
    private $transformation;

    /**
     * @var CfgFilter
     *
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\CfgFilter\CfgFilter")
     * @ORM\JoinColumn(name="cfg_filter_id", referencedColumnName="id", nullable=false)
     */
    private $cfgFilter;

    /**
     * @var int
     *
     * @ORM\Column(name="index", type="integer", nullable=false)
     */
    private $index;

    /**
     * @var object
     *
     * @ORM\Column(name="instance_values", type="json2", nullable=false)
     */
    private $instanceValues;

    /**
     * @param Transformation $transformation
     * @param CfgFilter      $cfgFilter
     * @param int            $index
     * @param object         $instanceValues
     */
    public function __construct(Transformation $transformation, CfgFilter $cfgFilter, int $index, object $instanceValues)
    {
        $this->transformation = $transformation;
        $this->cfgFilter = $cfgFilter;
        $this->index = $index;
        $this->instanceValues = $instanceValues;
    }

    /**
     * @return Transformation
     */
    public function transformation(): Transformation
    {
        return $this->transformation;
    }

    /**
     * @return CfgFilter
     */
    public function cfgFilter(): CfgFilter
    {
        return $this->cfgFilter;
    }

    /**
     * @return int
     */
    public function index(): int
    {
        return $this->index;
    }

    /**
     * @return object
     */
    public function instanceValues(): object
    {
        return $this->instanceValues;
    }
}
