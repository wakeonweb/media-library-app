<?php

namespace App\Entity\Transformation;

use App\Entity\CfgFilter\CfgFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\Transformation\TransformationRepository")
 * @ORM\Table(name="transformation", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"workspace_id", "name"})
 * })
 */
class Transformation
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var UuidInterface
     *
     * @ORM\Column(name="workspace_id", type="guid", nullable=false)
     */
    private $workspaceId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var TransformationFilter[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Transformation\TransformationFilter", mappedBy="transformation", cascade={"ALL"}, orphanRemoval=true, fetch="EAGER")
     * @ORM\OrderBy({
     *     "index": "ASC"
     * })
     */
    private $filters;

    /**
     * @param UuidInterface $id
     * @param UuidInterface $workspaceId
     * @param string        $name
     */
    public function __construct(UuidInterface $id, UuidInterface $workspaceId, string $name)
    {
        $this->id = $id;
        $this->workspaceId = $workspaceId;
        $this->name = $name;
        $this->filters = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return Uuid::fromString($this->workspaceId);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return TransformationFilter[]
     */
    public function filters(): array
    {
        return $this->filters->toArray();
    }

    /**
     * @param CfgFilter $cfgFilter
     * @param object    $instanceValues
     */
    public function pushFilter(CfgFilter $cfgFilter, object $instanceValues): void
    {
        $this->filters->add(new TransformationFilter(
            $this,
            $cfgFilter,
            $this->filters->count(),
            $instanceValues
        ));
    }
}
