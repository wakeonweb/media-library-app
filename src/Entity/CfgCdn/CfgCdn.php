<?php

namespace App\Entity\CfgCdn;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\CfgCdn\CfgCdnRepository")
 * @ORM\Table(name="cfg_cdn")
 */
class CfgCdn
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    private $name;

    /**
     * @var object
     *
     * @ORM\Column(name="model_schema", type="json2", nullable=false)
     */
    private $modelSchema;

    /**
     * @param UuidInterface $id
     * @param string        $name
     * @param object        $modelSchema
     */
    public function __construct(UuidInterface $id, string $name, object $modelSchema)
    {
        $this->id = $id;
        $this->name = $name;
        $this->modelSchema = $modelSchema;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return object
     */
    public function modelSchema(): object
    {
        return $this->modelSchema;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
