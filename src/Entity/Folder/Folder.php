<?php

namespace App\Entity\Folder;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\Folder\FolderRepository")
 * @ORM\Table(name="folder", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"parent_id", "name"})
 * })
 */
class Folder
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var UuidInterface
     *
     * @ORM\Column(name="workspace_id", type="guid", nullable=false)
     */
    private $workspaceId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    private $name;

    /**
     * @var Folder|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Folder\Folder")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @param UuidInterface $id
     * @param UuidInterface $workspaceId
     * @param string        $name
     * @param Folder|null   $parent
     */
    public function __construct(UuidInterface $id, UuidInterface $workspaceId, string $name, ?Folder $parent)
    {
        $this->id = $id;
        $this->workspaceId = $workspaceId;
        $this->name = $name;
        $this->parent = $parent;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return Uuid::fromString($this->workspaceId);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return Folder|null
     */
    public function parent(): ?Folder
    {
        return $this->parent;
    }
}
