<?php

namespace App\Entity\Cdn;

use App\Entity\CfgCdn\CfgCdn;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\Cdn\CdnRepository")
 * @ORM\Table(name="cdn", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"workspace_id", "cfg_cdn_id"})
 * })
 */
class Cdn
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var UuidInterface
     *
     * @ORM\Column(name="workspace_id", type="guid", nullable=false)
     */
    private $workspaceId;

    /**
     * @var CfgCdn
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CfgCdn\CfgCdn")
     * @ORM\JoinColumn(name="cfg_cdn_id", referencedColumnName="id", nullable=false)
     */
    private $cfgCdn;

    /**
     * @var object
     *
     * @ORM\Column(name="instance_values", type="json2", nullable=false)
     */
    private $instanceValues;

    /**
     * @param UuidInterface $id
     * @param UuidInterface $workspaceId
     * @param CfgCdn        $cfgCdn
     * @param object        $instanceValues
     */
    public function __construct(UuidInterface $id, UuidInterface $workspaceId, CfgCdn $cfgCdn, object $instanceValues)
    {
        $this->id = $id;
        $this->workspaceId = $workspaceId;
        $this->cfgCdn = $cfgCdn;
        $this->instanceValues = $instanceValues;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return UuidInterface
     */
    public function workspaceId(): UuidInterface
    {
        return $this->workspaceId;
    }

    /**
     * @return CfgCdn
     */
    public function cfgCdn(): CfgCdn
    {
        return $this->cfgCdn;
    }

    /**
     * @return object
     */
    public function instanceValues(): object
    {
        return $this->instanceValues;
    }
}
