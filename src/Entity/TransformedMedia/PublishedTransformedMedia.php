<?php

namespace App\Entity\TransformedMedia;

use App\Entity\Cdn\Cdn;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="published_transformed_media")
 */
class PublishedTransformedMedia
{
    /**
     * @var TransformedMedia
     *
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\TransformedMedia\TransformedMedia", inversedBy="publishedTransformedMedia")
     * @ORM\JoinColumn(name="transformed_media_id", referencedColumnName="id", nullable=false)
     */
    private $transformedMedia;

    /**
     * @var Cdn
     *
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Cdn\Cdn")
     * @ORM\JoinColumn(name="cdn_id", referencedColumnName="id", nullable=false)
     */
    private $cdn;

    /**
     * @var string
     *
     * @ORM\Column(name="public_url", type="string", nullable=false)
     */
    private $publicUrl;

    /**
     * @param TransformedMedia $transformedMedia
     * @param Cdn              $cdn
     * @param string           $publicUrl
     */
    public function __construct(TransformedMedia $transformedMedia, Cdn $cdn, string $publicUrl)
    {
        $this->transformedMedia = $transformedMedia;
        $this->cdn = $cdn;
        $this->publicUrl = $publicUrl;
    }

    /**
     * @return TransformedMedia
     */
    public function transformedMedia(): TransformedMedia
    {
        return $this->transformedMedia;
    }

    /**
     * @return Cdn
     */
    public function cdn(): Cdn
    {
        return $this->cdn;
    }

    /**
     * @return string
     */
    public function publicUrl(): string
    {
        return $this->publicUrl;
    }
}
