<?php

namespace App\Entity\TransformedMedia;

use App\Entity\Cdn\Cdn;
use App\Entity\Media\MediaTransformation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\TransformedMedia\TransformedMediaRepository")
 * @ORM\Table(name="transformed_media")
 */
class TransformedMedia
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var MediaTransformation
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media\MediaTransformation")
     * @ORM\JoinColumn(name="media_transformation_id", referencedColumnName="id", nullable=false)
     */
    private $mediaTransformation;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * @var int
     *
     * @ORM\Column(name="extension", type="integer", nullable=false)
     */
    private $extension;

    /**
     * @var PublishedTransformedMedia[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TransformedMedia\PublishedTransformedMedia", cascade={"ALL"}, orphanRemoval=true, mappedBy="transformedMedia")
     */
    private $publishedTransformedMedia;

    /**
     * @param UuidInterface       $id
     * @param MediaTransformation $mediaTransformation
     * @param int                 $size
     * @param string              $extension
     */
    public function __construct(UuidInterface $id, MediaTransformation $mediaTransformation, int $size, string $extension)
    {
        $this->id = $id;
        $this->mediaTransformation = $mediaTransformation;
        $this->size = $size;
        $this->extension = $extension;
        $this->publishedTransformedMedia = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return MediaTransformation
     */
    public function mediaTransformation(): MediaTransformation
    {
        return $this->mediaTransformation;
    }

    /**
     * @return int
     */
    public function size(): int
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function extension(): int
    {
        return $this->extension;
    }

    /**
     * @param Cdn    $cdn
     * @param string $publicUrl
     */
    public function publishOn(Cdn $cdn, string $publicUrl): void
    {
        $this->publishedTransformedMedia->add(new PublishedTransformedMedia($this, $cdn, $publicUrl));
    }
}
