<?php

namespace App\Entity\CfgFilter;

use App\Entity\CfgMimeType\CfgMimeType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\CfgFilter\CfgFilterRepository")
 * @ORM\Table(name="cfg_filter")
 */
class CfgFilter
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    private $name;

    /**
     * @var CfgMimeType[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\CfgMimeType\CfgMimeType")
     * @ORM\JoinTable(
     *     name="cfg_filter_mime_type",
     *     joinColumns={@ORM\JoinColumn(name="cfg_filter_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="cfg_mime_type_id", referencedColumnName="id", nullable=false)},
     * )
     */
    private $mimeTypes;

    /**
     * @var object
     *
     * @ORM\Column(name="model_schema", type="json2", nullable=false)
     */
    private $modelSchema;

    /**
     * @param UuidInterface $id
     * @param string        $name
     * @param CfgMimeType[] $mimeTypes
     * @param object        $modelSchema
     */
    public function __construct(UuidInterface $id, string $name, array $mimeTypes, object $modelSchema)
    {
        $this->id = $id;
        $this->name = $name;
        $this->mimeTypes = new ArrayCollection($mimeTypes);
        $this->modelSchema = $modelSchema;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return CfgMimeType[]
     */
    public function mimeTypes(): array
    {
        return $this->mimeTypes->toArray();
    }

    /**
     * @return object
     */
    public function modelSchema(): object
    {
        return $this->modelSchema;
    }

    /**
     * @param string $mimeType
     *
     * @return bool
     */
    public function isCompatibleWith(string $mimeType): bool
    {
        foreach ($this->mimeTypes as $cfgMimeType) {
            if ($cfgMimeType->equalsTo($mimeType)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
