<?php

namespace App\CommandHandler;

use App\Command\CreateCdn;
use App\Entity\Cdn\Cdn;
use App\Entity\CfgCdn\CfgCdn;
use App\Validator\JsonSchemaValidator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCdnCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var JsonSchemaValidator
     */
    private $validator;

    /**
     * @param RegistryInterface   $registry
     * @param JsonSchemaValidator $validator
     */
    public function __construct(RegistryInterface $registry, JsonSchemaValidator $validator)
    {
        $this->registry = $registry;
        $this->validator = $validator;
    }

    /**
     * @param CreateCdn $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateCdn $command): void
    {
        $repository = $this->registry->getRepository(Cdn::class);

        $cfgCdn = $this
            ->registry
            ->getRepository(CfgCdn::class)
            ->getByName($command->cfgCdn())
        ;

        $this->validator->validate($cfgCdn->modelSchema(), $instanceValues = $command->instanceValues());

        $cdn = new Cdn(
            $command->id(),
            $command->workspaceId(),
            $cfgCdn,
            $instanceValues
        );

        $repository->save($cdn);
    }
}
