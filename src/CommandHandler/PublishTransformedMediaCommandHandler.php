<?php

namespace App\CommandHandler;

use App\Command\PublishTransformedMedia;
use App\Entity\Cdn\Cdn;
use App\Entity\TransformedMedia\TransformedMedia;
use App\Publication\Publisher;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class PublishTransformedMediaCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var Publisher
     */
    private $publisher;

    /**
     * @param RegistryInterface $registry
     * @param Publisher         $publisher
     */
    public function __construct(RegistryInterface $registry, Publisher $publisher)
    {
        $this->registry = $registry;
        $this->publisher = $publisher;
    }

    /**
     * @param PublishTransformedMedia $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(PublishTransformedMedia $command): void
    {
        $repository = $this->registry->getRepository(TransformedMedia::class);

        $transformedMedia = $repository->find($command->id());

        $cdn = $this
            ->registry
            ->getRepository(Cdn::class)
            ->find($command->cdnId())
        ;

        $publicUrl = $this->publisher->publish($transformedMedia, $cdn);

        $transformedMedia->publishOn($cdn, $publicUrl);

        $repository->save($transformedMedia);
    }
}
