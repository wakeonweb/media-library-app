<?php

namespace App\CommandHandler;

use App\Command\TransformMedia;
use App\Entity\Media\MediaTransformation;
use App\Entity\TransformedMedia\TransformedMedia;
use App\Event\MediaWasTransformed;
use App\Transformation\Transformer;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use SplFileInfo;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class TransformMediaCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var MessageBusInterface
     */
    private $eventBus;

    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * @param RegistryInterface   $registry
     * @param MessageBusInterface $eventBus
     * @param Transformer         $transformer
     */
    public function __construct(RegistryInterface $registry, MessageBusInterface $eventBus, Transformer $transformer)
    {
        $this->registry = $registry;
        $this->eventBus = $eventBus;
        $this->transformer = $transformer;
    }

    /**
     * @param TransformMedia $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function __invoke(TransformMedia $command): void
    {
        $mediaTransformation = $this
            ->registry
            ->getRepository(MediaTransformation::class)
            ->get($command->mediaTransformationId())
        ;

        $file = new SplFileInfo($this->transformer->transform(
            $mediaTransformation->media(),
            $mediaTransformation->transformation(),
            $command->id()
        ));

        $repository = $this->registry->getRepository(TransformedMedia::class);

        $transformedMedia = new TransformedMedia(
            $command->id(),
            $mediaTransformation,
            $file->getSize(),
            $file->getExtension()
        );

        $repository->save($transformedMedia);

        $this->eventBus->dispatch(new MediaWasTransformed($transformedMedia->id()));
    }
}
