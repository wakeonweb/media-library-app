<?php

namespace App\CommandHandler;

use App\Command\CreateCfgMimeType;
use App\Entity\CfgMimeType\CfgMimeType;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgMimeTypeCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param CreateCfgMimeType $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateCfgMimeType $command): void
    {
        $repository = $this->registry->getRepository(CfgMimeType::class);

        $cfgMimeType = new CfgMimeType(
            $command->id(),
            $command->name()
        );

        $repository->save($cfgMimeType);
    }
}
