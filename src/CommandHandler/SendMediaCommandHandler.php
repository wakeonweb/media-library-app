<?php

namespace App\CommandHandler;

use App\Command\SendMedia;
use App\Entity\CfgMimeType\CfgMimeType;
use App\Entity\Folder\Folder;
use App\Entity\Media\Media;
use App\Entity\Transformation\Transformation;
use App\Event\MediaTransformationWasAdded;
use App\Utils\PathUtils;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class SendMediaCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var MessageBusInterface
     */
    private $eventBus;

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @param RegistryInterface   $registry
     * @param MessageBusInterface $eventBus
     * @param string              $uploadDir
     */
    public function __construct(RegistryInterface $registry, MessageBusInterface $eventBus, string $uploadDir)
    {
        $this->registry = $registry;
        $this->eventBus = $eventBus;
        $this->uploadDir = $uploadDir;
    }

    /**
     * @param SendMedia $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function __invoke(SendMedia $command): void
    {
        $repository = $this->registry->getRepository(Media::class);

        $file = $command->file();

        $cfgMimeType = $this
            ->registry
            ->getRepository(CfgMimeType::class)
            ->getByName($file->getMimeType())
        ;

        $originalFilename = $file->getClientOriginalName();
        $originalExtension = $file->getClientOriginalExtension();
        $size = $file->getSize();

        $file->move(...PathUtils::uuidToPathAndFilename((string) $command->id(), sprintf('%s/media', $this->uploadDir), $originalExtension));

        if ($command->isPlacedInFolder()) {
            $folder = $this
                ->registry
                ->getRepository(Folder::class)
                ->find($command->folderId())
            ;
        } else  {
            $folder = null;
        }

        $media = new Media(
            $command->id(),
            $command->workspaceId(),
            $cfgMimeType,
            $size,
            pathinfo($originalFilename, PATHINFO_FILENAME),
            $originalExtension,
            $command->tags(),
            $command->description(),
            $folder
        );

        foreach ($command->transformations() as $dto) {
            $transformation = $this
                ->registry
                ->getRepository(Transformation::class)
                ->getByNameInWorkspace($dto->transformation(), $command->workspaceId())
            ;

            $media->addTransformation($transformation);
        }

        $repository->save($media);

        foreach ($media->mediaTransformations() as $transformation) {
            $this->eventBus->dispatch(new MediaTransformationWasAdded($transformation->id()));
        }
    }
}
