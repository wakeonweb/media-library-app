<?php

namespace App\CommandHandler;

use App\Command\CreateRootFolder;
use App\Entity\Folder\Folder;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateRootFolderCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param CreateRootFolder $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateRootFolder $command): void
    {
        $repository = $this->registry->getRepository(Folder::class);

        $folder = new Folder(
            $command->id(),
            $command->workspaceId(),
            $command->name(),
            null
        );

        $repository->save($folder);
    }
}
