<?php

namespace App\CommandHandler;

use App\Command\CreateNestedFolder;
use App\Entity\Folder\Folder;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateNestedFolderCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param CreateNestedFolder $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateNestedFolder $command): void
    {
        $repository = $this->registry->getRepository(Folder::class);

        $parent = $repository->find($command->parentId());

        $folder = new Folder(
            $command->id(),
            $command->workspaceId(),
            $command->name(),
            $parent
        );

        $repository->save($folder);
    }
}
