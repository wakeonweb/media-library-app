<?php

namespace App\CommandHandler;

use App\Command\CreateCfgCdn;
use App\Entity\CfgCdn\CfgCdn;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgCdnCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param CreateCfgCdn $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateCfgCdn $command): void
    {
        $repository = $this->registry->getRepository(CfgCdn::class);

        $cfgFilter = new CfgCdn(
            $command->id(),
            $command->name(),
            $command->modelSchema()
        );

        $repository->save($cfgFilter);
    }
}
