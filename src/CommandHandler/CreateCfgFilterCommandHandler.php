<?php

namespace App\CommandHandler;

use App\Command\CreateCfgFilter;
use App\Entity\CfgFilter\CfgFilter;
use App\Entity\CfgMimeType\CfgMimeType;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgFilterCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param CreateCfgFilter $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateCfgFilter $command)
    {
        $repository = $this->registry->getRepository(CfgFilter::class);

        $mimeTypes = $this
            ->registry
            ->getRepository(CfgMimeType::class)
            ->findMimeTypesByNames($command->mimeTypes())
        ;

        $cfgFilter = new CfgFilter(
            $command->id(),
            $command->name(),
            $mimeTypes,
            $command->modelSchema()
        );

        $repository->save($cfgFilter);
    }
}
