<?php

namespace App\CommandHandler;

use App\Command\CreateTransformation;
use App\Entity\CfgFilter\CfgFilter;
use App\Entity\Transformation\Transformation;
use App\Validator\JsonSchemaValidator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateTransformationCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var JsonSchemaValidator
     */
    private $validator;

    /**
     * @param RegistryInterface   $registry
     * @param JsonSchemaValidator $validator
     */
    public function __construct(RegistryInterface $registry, JsonSchemaValidator $validator)
    {
        $this->registry = $registry;
        $this->validator = $validator;
    }

    /**
     * @param CreateTransformation $command
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(CreateTransformation $command): void
    {
        $repository = $this->registry->getRepository(Transformation::class);

        $transformation = new Transformation(
            $command->id(),
            $command->workspaceId(),
            $command->name()
        );

        foreach ($command->filters() as $dto) {
            $cfgFilter = $this
                ->registry
                ->getRepository(CfgFilter::class)
                ->getByName($dto->cfgFilter())
            ;

            $this->validator->validate($cfgFilter->modelSchema(), $instanceValues = $dto->instanceValues());

            $transformation->pushFilter($cfgFilter, $instanceValues);
        }

        $repository->save($transformation);
    }
}
