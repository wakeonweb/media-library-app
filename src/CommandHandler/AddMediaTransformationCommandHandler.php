<?php

namespace App\CommandHandler;

use App\Command\AddMediaTransformation;
use App\Entity\Media\Media;
use App\Entity\Transformation\Transformation;
use App\Event\MediaTransformationWasAdded;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class AddMediaTransformationCommandHandler
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var MessageBusInterface
     */
    private $eventBus;

    /**
     * @param RegistryInterface   $registry
     * @param MessageBusInterface $eventBus
     */
    public function __construct(RegistryInterface $registry, MessageBusInterface $eventBus)
    {
        $this->registry = $registry;
        $this->eventBus = $eventBus;
    }

    /**
     * @param AddMediaTransformation $command
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(AddMediaTransformation $command): void
    {
        $repository = $this->registry->getRepository(Media::class);

        $media = $repository->find($command->mediaId());

        $transformation = $this
            ->registry
            ->getRepository(Transformation::class)
            ->getByNameInWorkspace($command->transformation(), $command->workspaceId())
        ;

        $id = $media->addTransformation($transformation);

        $repository->save($media);

        $this->eventBus->dispatch(new MediaTransformationWasAdded($id));
    }
}
