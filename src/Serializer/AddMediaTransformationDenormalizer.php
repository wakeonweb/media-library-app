<?php

namespace App\Serializer;

use App\Command\AddMediaTransformation;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class AddMediaTransformationDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $attributes = $context['request']->attributes->all();

        return new AddMediaTransformation(
            Uuid::fromString($attributes['workspace_id']),
            Uuid::fromString($attributes['id']),
            $data['name']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === AddMediaTransformation::class;
    }
}
