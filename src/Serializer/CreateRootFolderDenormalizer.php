<?php

namespace App\Serializer;

use App\Command\CreateRootFolder;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateRootFolderDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $attributes = $context['request']->attributes->all();

        return new CreateRootFolder(
            Uuid::fromString($attributes['workspace_id']),
            Uuid::uuid4(),
            $data['name']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === CreateRootFolder::class;
    }
}
