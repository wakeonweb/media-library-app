<?php

namespace App\Serializer;

use App\Command\CreateTransformation;
use App\Command\DTO\TransformationFilter;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateTransformationDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $attributes = $context['request']->attributes->all();

        return new CreateTransformation(
            Uuid::fromString($attributes['workspace_id']),
            Uuid::uuid4(),
            $data['name'],
            array_map(
                function (array $data) {
                    return new TransformationFilter(
                        $data['cfg_filter'],
                        (object) $data['instance_values']
                    );
                },
                $data['filters']
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === CreateTransformation::class;
    }
}
