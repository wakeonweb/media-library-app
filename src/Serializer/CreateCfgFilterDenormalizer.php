<?php

namespace App\Serializer;

use App\Command\CreateCfgFilter;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgFilterDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        return new CreateCfgFilter(
            Uuid::uuid4(),
            $data['name'],
            $data['mime_types'],
            (object) $data['model_schema']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === CreateCfgFilter::class;
    }
}
