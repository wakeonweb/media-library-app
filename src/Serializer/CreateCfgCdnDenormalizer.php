<?php

namespace App\Serializer;

use App\Command\CreateCfgCdn;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgCdnDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        return new CreateCfgCdn(
            Uuid::uuid4(),
            $data['name'],
            (object) $data['model_schema']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === CreateCfgCdn::class;
    }
}
