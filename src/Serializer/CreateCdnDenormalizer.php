<?php

namespace App\Serializer;

use App\Command\CreateCdn;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCdnDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $attributes = $context['request']->attributes->all();

        return new CreateCdn(
            Uuid::fromString($attributes['workspace_id']),
            Uuid::uuid4(),
            $data['cfg_cdn'],
            (object) $data['instance_values']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === CreateCdn::class;
    }
}
