<?php

namespace App\Serializer;

use App\Command\CreateCfgMimeType;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CreateCfgMimeTypeDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        return new CreateCfgMimeType(
            Uuid::uuid4(),
            $data['name']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === CreateCfgMimeType::class;
    }
}
