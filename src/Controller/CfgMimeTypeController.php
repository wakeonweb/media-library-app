<?php

namespace App\Controller;

use App\Command\CreateCfgMimeType;
use App\Query\FindCfgMimeTypeById;
use App\Query\ListCfgMimeTypes;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @Route("/api/cfg-mime-types", name="api.cfg_mime_type.")
 */
class CfgMimeTypeController
{
    /**
     * @Route("", methods={"GET"}, name="list")
     *
     * @param MessageBusInterface $queryBus
     *
     * @return Response
     */
    public function list(MessageBusInterface $queryBus): Response
    {
        $data = $queryBus->dispatch(new ListCfgMimeTypes());

        return new Response($data, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/{id}", methods={"GET"}, name="show")
     *
     * @param Request             $request
     * @param MessageBusInterface $queryBus
     *
     * @return Response
     */
    public function show(Request $request, MessageBusInterface $queryBus): Response
    {
        $data = $queryBus->dispatch(FindCfgMimeTypeById::fromRequest($request));

        if (!$data) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return new Response($data, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * @param Request               $request
     * @param SerializerInterface   $serializer
     * @param MessageBusInterface   $commandBus
     * @param UrlGeneratorInterface $urlGenerator
     *
     * @return Response
     *
     * @Route("", methods={"POST"}, name="create")
     */
    public function create(Request $request, SerializerInterface $serializer, MessageBusInterface $commandBus, UrlGeneratorInterface $urlGenerator): Response
    {
        try {
            /** @var CreateCfgMimeType $command */
            $command = $serializer->deserialize($request->getContent(), CreateCfgMimeType::class, 'json');
            $commandBus->dispatch($command);
        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return new Response('', Response::HTTP_CREATED, [
            'Location' => $urlGenerator->generate('api.cfg_mime_type.show', [
                'id' => (string) $command->id(),
            ]),
        ]);
    }
}
