<?php

namespace App\Controller;

use App\Command\AddMediaTransformation;
use App\Command\DTO\MediaTransformation;
use App\Command\SendMedia;
use App\Query\FindMediaById;
use App\Query\ListMedia;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @Route("/api/workspaces/{workspace_id}/media", name="api.media.")
 */
class MediaController
{
    /**
     * @Route("", methods={"GET"}, name="list")
     *
     * @param Request             $request
     * @param MessageBusInterface $queryBus
     *
     * @return Response
     */
    public function list(Request $request, MessageBusInterface $queryBus): Response
    {
        $data = $queryBus->dispatch(ListMedia::fromRequest($request));

        if (!$data) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return new Response($data, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/{id}", methods={"GET"}, name="show")
     *
     * @param Request             $request
     * @param MessageBusInterface $queryBus
     *
     * @return Response
     */
    public function show(Request $request, MessageBusInterface $queryBus): Response
    {
        $data = $queryBus->dispatch(FindMediaById::fromRequest($request));

        if (!$data) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return new Response($data, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * @param Request               $request
     * @param MessageBusInterface   $commandBus
     * @param UrlGeneratorInterface $urlGenerator
     *
     * @return Response
     *
     * @Route("", methods={"POST"}, name="create")
     */
    public function create(Request $request, MessageBusInterface $commandBus, UrlGeneratorInterface $urlGenerator): Response
    {
        try {
            $command = new SendMedia(
                Uuid::fromString($request->attributes->get('workspace_id')),
                Uuid::uuid4(),
                $request->files->get('file'),
                $request->request->get('tags', []),
                $request->request->get('description', null),
                $request->request->has('folder_id') ? Uuid::fromString($request->request->get('folder_id')) : null,
                array_map(
                    function (string $transformation) {
                        return new MediaTransformation($transformation);
                    },
                    $request->request->get('transformations', [])
                )
            );

            $commandBus->dispatch($command);
        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return new Response('', Response::HTTP_CREATED, [
            'Location' => $urlGenerator->generate('api.media.show', [
                'workspace_id' => (string) $command->workspaceId(),
                'id' => (string) $command->id(),
            ]),
        ]);
    }

    /**
     * @Route("/{id}/transform", methods={"POST"}, name="transform")
     *
     * @param Request             $request
     * @param SerializerInterface $serializer
     * @param MessageBusInterface $commandBus
     *
     * @return Response
     */
    public function transform(Request $request, SerializerInterface $serializer, MessageBusInterface $commandBus): Response
    {
        try {
            /** @var AddMediaTransformation $command */
            $command = $serializer->deserialize($request->getContent(), AddMediaTransformation::class, 'json', ['request' => $request]);
            $commandBus->dispatch($command);
        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
