<?php

namespace App\Controller;

use App\Query\ListTransformedMedia;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 *
 * @Route("/api/workspaces/{workspace_id}/media/{media_id}/transformed-media", name="api.transformed_media.")
 */
class TransformedMediaController
{
    /**
     * @Route("", methods={"GET"}, name="list")
     *
     * @param Request             $request
     * @param MessageBusInterface $queryBus
     *
     * @return Response
     */
    public function list(Request $request, MessageBusInterface $queryBus): Response
    {
        $data = $queryBus->dispatch(ListTransformedMedia::fromRequest($request));

        if (!$data) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return new Response($data, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }
}
