<?php

namespace App\Validator;

use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class JsonSchemaValidator
{
    /**
     * @param $schema
     * @param $value
     */
    public function validate($schema, $value): void
    {
        $validator = new Validator();
        $validator->validate($value, $schema, Constraint::CHECK_MODE_EXCEPTIONS);

        dump($validator->getErrors());
    }
}
