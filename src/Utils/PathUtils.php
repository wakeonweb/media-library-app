<?php

namespace App\Utils;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class PathUtils
{
    /**
     * @param string      $uuid
     * @param string|null $basePath
     * @param string|null $extension
     *
     * @return string
     */
    public static function uuidToPath(string $uuid, string $basePath = null, string $extension = null): string
    {
        return sprintf('%s/%s', ...self::uuidToPathAndFilename($uuid, $basePath, $extension));
    }

    /**
     * @param string      $uuid
     * @param string|null $basePath
     * @param string|null $extension
     *
     * @return string[]
     */
    public static function uuidToPathAndFilename(string $uuid, string $basePath = null, string $extension = null): array
    {
        $filename = substr($uuid, 4);
        $path = sprintf('%s/%s', substr($uuid, 0, 2), substr($uuid, 2, 2));

        if ($basePath !== null) {
            $path = sprintf('%s/%s', rtrim($basePath, '/'), $path);
        }

        if ($extension !== null) {
            $filename = sprintf('%s.%s', $filename, $extension);
        }

        return [$path, $filename];
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public static function pathToPublic(string $path): string
    {
        $segments = explode('/', $path);
        $segments = \array_slice($segments, -3);

        return sprintf('media/%s', implode('/', $segments));
    }
}
