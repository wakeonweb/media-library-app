<?php

namespace App\Transformation\Filterable;

use App\Transformation\Filterable;
use App\Utils\PathUtils;
use Imagine\Image\ImageInterface;
use LogicException;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class ImagineBridge implements Filterable
{
    /**
     * @var ImageInterface
     */
    private $image;

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var string
     */
    private $outputPath;

    /**
     * @var array
     */
    private $saveOptions = [];

    /**
     * @param ImageInterface $image
     * @param string         $mimeType
     * @param string         $outputPath
     */
    public function __construct(ImageInterface $image, string $mimeType, string $outputPath)
    {
        $this->image = $image;
        $this->mimeType = $mimeType;
        $this->outputPath = $outputPath;
    }

    /**
     * {@inheritdoc}
     */
    public function mimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * {@inheritdoc}
     */
    public function save(UuidInterface $transformedMediaId): string
    {
        [$path, $filename] = PathUtils::uuidToPathAndFilename((string) $transformedMediaId, $this->outputPath, $this->guessExtension());

        $fs = new Filesystem();
        $fs->mkdir($path);

        $this->image->save($path = sprintf('%s/%s', $path, $filename), $this->saveOptions);

        return $path;
    }

    /**
     * @param int $quality
     */
    public function optimizeJpeg(int $quality): void
    {
        $this->saveOptions = ['jpeg_quality' => $quality];
        $this->mimeType = 'image/jpeg';
    }

    /**
     * @param int $compressionLevel
     */
    public function optimizePng(int $compressionLevel): void
    {
        $this->saveOptions = ['png_compression_level' => $compressionLevel];
        $this->mimeType = 'image/png';
    }

    /**
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public function __call(string $name, array $arguments)
    {
        return $this->image->{$name}($arguments);
    }

    /**
     * @return string
     */
    private function guessExtension(): string
    {
        switch ($this->mimeType) {
            case 'image/gif':
                return 'gif';

            case 'image/jpeg':
                return 'jpg';

            case 'image/png':
                return 'png';

            case 'image/vnd.wap.wbmp':
                return 'wbmp';

            case 'image/x-xbitmap':
                return 'xbm';

            default:
                throw new LogicException(sprintf(
                    'The mime type "%s" doen\'t seems to be compatible image mime type.',
                    $this->mimeType
                ));
        }
    }
}
