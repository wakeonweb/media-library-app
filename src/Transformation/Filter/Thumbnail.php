<?php

namespace App\Transformation\Filter;

use App\Transformation\Filterable;
use App\Transformation\Filter;
use Imagine\Image\Box;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Thumbnail implements Filter
{
    /**
     * {@inheritdoc}
     */
    public function apply(Filterable $image, object $values): Filterable
    {
        $image->thumbnail(
            new Box(
                $values->{'size'}->{'width'},
                $values->{'size'}->{'height'}
            ),
            $values->{'mode'}
        );

        return $image;
    }
}
