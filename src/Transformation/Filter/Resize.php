<?php

namespace App\Transformation\Filter;

use App\Transformation\Filterable;
use App\Transformation\Filter;
use Imagine\Image\Box;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Resize implements Filter
{
    /**
     * {@inheritdoc}
     */
    public function apply(Filterable $image, object $values): Filterable
    {
        $image->resize(new Box(
            $values->{'box'}->{'width'},
            $values->{'box'}->{'height'}
        ));

        return $image;
    }
}
