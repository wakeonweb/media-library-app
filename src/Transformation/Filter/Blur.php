<?php

namespace App\Transformation\Filter;

use App\Transformation\Filterable;
use App\Transformation\Filter;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Blur implements Filter
{
    /**
     * {@inheritdoc}
     */
    public function apply(Filterable $image, object $values): Filterable
    {
        $image->effects()->blur($values->{'sigma'});

        return $image;
    }
}
