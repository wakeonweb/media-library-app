<?php

namespace App\Transformation\Filter;

use App\Transformation\Filterable;
use App\Transformation\Filter;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Flip implements Filter
{
    /**
     * {@inheritdoc}
     */
    public function apply(Filterable $image, object $values): Filterable
    {
        if ($values->{'horizontally'} === true) {
            $image->flipHorizontally();
        }

        if ($values->{'vertically'} === true) {
            $image->flipVertically();
        }

        return $image;
    }
}
