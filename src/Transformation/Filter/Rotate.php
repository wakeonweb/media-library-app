<?php

namespace App\Transformation\Filter;

use App\Transformation\Filterable;
use App\Transformation\Filter;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Rotate implements Filter
{
    /**
     * {@inheritdoc}
     */
    public function apply(Filterable $image, object $values): Filterable
    {
        if (property_exists($values, 'background')) {
            $background = $image->palette()->color($values->{'background'});
        } else {
            $background = null;
        }

        $image->rotate($values->{'angle'}, $background);

        return $image;
    }
}
