<?php

namespace App\Transformation\Filter;

use App\Transformation\Filterable;
use App\Transformation\Filter;
use Imagine\Image\Box;
use Imagine\Image\Point;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Crop implements Filter
{
    /**
     * {@inheritdoc}
     */
    public function apply(Filterable $image, object $values): Filterable
    {
        $image->crop(
            new Point(
                $values->{'start'}->{'x'},
                $values->{'start'}->{'y'}
            ),
            new Box(
                $values->{'size'}->{'width'},
                $values->{'size'}->{'height'}
            )
        );

        return $image;
    }
}
