<?php

namespace App\Transformation\Filter;

use App\Transformation\Filterable;
use App\Transformation\Filter;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Sharpen implements Filter
{
    /**
     * {@inheritdoc}
     */
    public function apply(Filterable $image, object $values): Filterable
    {
        $image->effects()->sharpen();

        return $image;
    }
}
