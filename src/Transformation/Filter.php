<?php

namespace App\Transformation;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
interface Filter
{
    /**
     * @param Filterable $media
     * @param object     $values
     *
     * @return Filterable
     */
    public function apply(Filterable $media, object $values): Filterable;
}
