<?php

namespace App\Transformation;

use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
interface Filterable
{
    /**
     * @return string
     */
    public function mimeType(): string;

    /**
     * @param UuidInterface $transformedMediaId
     *
     * @return string
     */
    public function save(UuidInterface $transformedMediaId): string;
}
