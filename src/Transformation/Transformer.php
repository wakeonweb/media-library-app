<?php

namespace App\Transformation;

use App\Entity\CfgMimeType\CfgMimeType;
use App\Entity\Media\Media;
use App\Entity\Transformation\Transformation;
use App\Transformation\Filterable\ImagineBridge;
use App\Utils\PathUtils;
use Imagine\Gd\Imagine;
use LogicException;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class Transformer
{
    /**
     * @var ContainerInterface
     */
    private $filters;

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @param ContainerInterface $filters
     * @param string             $uploadDir
     */
    public function __construct(ContainerInterface $filters, string $uploadDir)
    {
        $this->filters = $filters;
        $this->uploadDir = $uploadDir;
    }

    /**
     * @param Media          $media
     * @param Transformation $transformation
     * @param UuidInterface  $transformedMediaId
     *
     * @return string|null
     */
    public function transform(Media $media, Transformation $transformation, UuidInterface $transformedMediaId): ?string
    {
        $filterable = $this->mediaToFilterable($media);

        $filters = $transformation->filters();

        while ($filter = array_shift($filters)) {
            $cfgFilter = $filter->cfgFilter();

            if (!$cfgFilter->isCompatibleWith($filterable->mimeType())) {
                throw new LogicException(sprintf(
                    'The CfgFilter named "%s" is not compatible with the mime type "%s".',
                    (string)$cfgFilter,
                    (string)$media->cfgMimeType())
                );
            }

            $filterable = $this
                ->filters
                ->get((string) $filter->cfgFilter())
                ->apply($filterable, $filter->instanceValues())
            ;
        }

        return $filterable->save($transformedMediaId);
    }

    /**
     * @param Media $media
     *
     * @return Filterable
     */
    private function mediaToFilterable(Media $media): Filterable
    {
        $path = sprintf('%s/transformed-media', $this->uploadDir);

        switch ($media->cfgMimeType()->guessType()) {
            case CfgMimeType::TYPE_IMAGE:
                return new ImagineBridge(
                    (new Imagine())->open(PathUtils::uuidToPath((string) $media->id(), sprintf('%s/media', $this->uploadDir), $media->originalExtension())),
                    (string) $media->cfgMimeType(),
                    $path
                );

            case CfgMimeType::TYPE_TEXT:
            case CfgMimeType::TYPE_VIDEO:
            case CfgMimeType::TYPE_AUDIO:
            default:
                return null;
        }
    }
}
