<?php

namespace App\Repository\CfgFilter;

use App\Entity\CfgFilter\CfgFilter;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CfgFilterRepository extends EntityRepository
{
    /**
     * @param string $name
     *
     * @return CfgFilter
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getByName(string $name): CfgFilter
    {
        return $this
            ->createQueryBuilder('cf')
            ->where('cf.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleResult()
        ;
    }

    /**
     * @param CfgFilter $cfgFilter
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(CfgFilter $cfgFilter): void
    {
        if (!$this->_em->contains($cfgFilter)) {
            $this->_em->persist($cfgFilter);
        }

        $this->_em->flush();
    }
}
