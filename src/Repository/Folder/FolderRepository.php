<?php

namespace App\Repository\Folder;

use App\Entity\Folder\Folder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class FolderRepository extends EntityRepository
{
    /**
     * @param Folder $folder
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Folder $folder): void
    {
        if (!$this->_em->contains($folder)) {
            $this->_em->persist($folder);
        }

        $this->_em->flush();
    }
}
