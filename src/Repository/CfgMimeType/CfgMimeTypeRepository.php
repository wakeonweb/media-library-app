<?php

namespace App\Repository\CfgMimeType;

use App\Entity\CfgMimeType\CfgMimeType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CfgMimeTypeRepository extends EntityRepository
{
    /**
     * @param string $name
     *
     * @return CfgMimeType
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getByName(string $name): CfgMimeType
    {
        return $this
            ->createQueryBuilder('cmt')
            ->where('cmt.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleResult()
        ;
    }

    /**
     * @param string[] $names
     *
     * @return CfgMimeType[]
     */
    public function findMimeTypesByNames(array $names): array
    {
        return $this
            ->createQueryBuilder('mt')
            ->where('mt.name IN (:names)')
            ->getQuery()
            ->setParameters([
                'names' => $names,
            ])
            ->getResult()
        ;
    }

    /**
     * @param CfgMimeType $cfgMimeType
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(CfgMimeType $cfgMimeType): void
    {
        if (!$this->_em->contains($cfgMimeType)) {
            $this->_em->persist($cfgMimeType);
        }

        $this->_em->flush();
    }
}
