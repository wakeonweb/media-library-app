<?php

namespace App\Repository\Transformation;

use App\Entity\Transformation\Transformation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class TransformationRepository extends EntityRepository
{
    /**
     * @param string        $name
     * @param UuidInterface $workspaceId
     *
     * @return Transformation
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getByNameInWorkspace(string $name, UuidInterface $workspaceId): Transformation
    {
        return $this
            ->createQueryBuilder('t')
            ->where('t.workspaceId = :workspace_id AND t.name = :name')
            ->setParameters([
                'workspace_id' => $workspaceId,
                'name' => $name,
            ])
            ->getQuery()
            ->getSingleResult()
        ;
    }

    /**
     * @param Transformation $transformation
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Transformation $transformation): void
    {
        if (!$this->_em->contains($transformation)) {
            $this->_em->persist($transformation);
        }

        $this->_em->flush();
    }
}
