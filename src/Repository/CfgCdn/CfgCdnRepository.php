<?php

namespace App\Repository\CfgCdn;

use App\Entity\CfgCdn\CfgCdn;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CfgCdnRepository extends EntityRepository
{
    /**
     * @param string $name
     *
     * @return CfgCdn
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getByName(string $name): CfgCdn
    {
        return $this
            ->createQueryBuilder('cc')
            ->where('cc.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleResult()
        ;
    }

    /**
     * @param CfgCdn $cfgCdn
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(CfgCdn $cfgCdn): void
    {
        if (!$this->_em->contains($cfgCdn)) {
            $this->_em->persist($cfgCdn);
        }

        $this->_em->flush();
    }
}
