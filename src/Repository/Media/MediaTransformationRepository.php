<?php

namespace App\Repository\Media;

use App\Entity\Media\MediaTransformation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Ramsey\Uuid\UuidInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class MediaTransformationRepository extends EntityRepository
{
    /**
     * @param UuidInterface $id
     *
     * @return MediaTransformation
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function get(UuidInterface $id): MediaTransformation
    {
        return $this
            ->createQueryBuilder('mt')
            ->where('mt.id = :id')
            ->setParameter('id', (string) $id)
            ->getQuery()
            ->getSingleResult()
        ;
    }
}
