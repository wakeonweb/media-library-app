<?php

namespace App\Repository\Media;

use App\Entity\Media\Media;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class MediaRepository extends EntityRepository
{
    /**
     * @param Media $media
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Media $media): void
    {
        if (!$this->_em->contains($media)) {
            $this->_em->persist($media);
        }

        $this->_em->flush();
    }
}
