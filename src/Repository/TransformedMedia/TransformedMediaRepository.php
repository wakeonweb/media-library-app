<?php

namespace App\Repository\TransformedMedia;

use App\Entity\TransformedMedia\TransformedMedia;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class TransformedMediaRepository extends EntityRepository
{
    /**
     * @param TransformedMedia $transformedMedia
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(TransformedMedia $transformedMedia): void
    {
        if (!$this->_em->contains($transformedMedia)) {
            $this->_em->persist($transformedMedia);
        }

        $this->_em->flush();
    }
}
