<?php

namespace App\Repository\Cdn;

use App\Entity\Cdn\Cdn;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class CdnRepository extends EntityRepository
{
    /**
     * @param Cdn $cdn
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Cdn $cdn): void
    {
        if (!$this->_em->contains($cdn)) {
            $this->_em->persist($cdn);
        }

        $this->_em->flush();
    }
}
