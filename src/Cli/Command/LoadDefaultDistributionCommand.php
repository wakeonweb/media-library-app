<?php

namespace App\Cli\Command;

use App\Command\CreateCfgCdn;
use App\Command\CreateCfgFilter;
use App\Command\CreateCfgMimeType;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class LoadDefaultDistributionCommand extends Command
{
    /**
     * @var MessageBusInterface
     */
    private $commandBus;

    /**
     * @var string
     */
    private $projectDir;

    /**
     * @param MessageBusInterface $commandBus
     * @param string              $projectDir
     */
    public function __construct(MessageBusInterface $commandBus, string $projectDir)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
        $this->projectDir = $projectDir;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('app:load-default-distribution')
            ->setDescription('Load the default configuration of the media library service.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->commandBus->dispatch(new CreateCfgCdn(
            Uuid::uuid4(),
            'media-library',
            $this->getSchema('cfg-cdn.media-library.json')
        ));

        foreach ([
            'image/jpeg',
            'image/png',
        ] as $mimeType) {
            $this->commandBus->dispatch(new CreateCfgMimeType(Uuid::uuid4(), $mimeType));
        }

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'blur',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.blur.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'colorize',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.colorize.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'crop',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.crop.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'flip',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.flip.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'gamma',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.gamma.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'grayscale',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.grayscale.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'negative',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.negative.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'optimize-jpeg',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.optimize-jpeg.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'optimize-png',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.optimize-png.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'resize',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.resize.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'rotate',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.rotate.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'sharpen',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.sharpen.json')
        ));

        $this->commandBus->dispatch(new CreateCfgFilter(
            Uuid::uuid4(),
            'thumbnail',
            ['image/jpeg', 'image/png'],
            $this->getSchema('cfg-filter.thumbnail.json')
        ));
    }

    /**
     * @param string $filename
     *
     * @return object
     */
    private function getSchema(string $filename): object
    {
        return json_decode(file_get_contents(sprintf('%s/res/json-schema/%s', $this->projectDir, $filename)));
    }
}
