<?php

namespace App\EventHandler;

use App\Command\TransformMedia;
use App\Event\MediaTransformationWasAdded;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class MediaTransformationWasAddedEventHandler
{
    /**
     * @var MessageBusInterface
     */
    private $commandBus;

    /**
     * @param MessageBusInterface $commandBus
     */
    public function __construct(MessageBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @param MediaTransformationWasAdded $event
     *
     * @throws Exception
     */
    public function __invoke(MediaTransformationWasAdded $event): void
    {
        $this->commandBus->dispatch(new TransformMedia(Uuid::uuid4(), $event->id()));
    }
}
