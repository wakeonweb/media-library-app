<?php

namespace App\EventHandler;

use App\Command\PublishTransformedMedia;
use App\Event\MediaWasTransformed;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @author Quentin Schuler <qschuler@neosyne.com>
 */
class MediaWasTransformedEventHandler
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var MessageBusInterface
     */
    private $commandBus;

    /**
     * @param Connection          $connection
     * @param MessageBusInterface $commandBus
     */
    public function __construct(Connection $connection, MessageBusInterface $commandBus)
    {
        $this->connection = $connection;
        $this->commandBus = $commandBus;
    }

    /**
     * @param MediaWasTransformed $event
     *
     * @throws DBALException
     */
    public function __invoke(MediaWasTransformed $event): void
    {
        $statement = $this->connection->prepare(file_get_contents(__DIR__.'/find-cdns-in-same-workspace-of-transformed-media.sql'));
        $statement->execute([
            'id' => (string) $event->id(),
        ]);

        $ids = $statement->fetchAll(FetchMode::COLUMN);

        foreach ($ids as $id) {
            $this->commandBus->dispatch(new PublishTransformedMedia(
                Uuid::fromString($id),
                $event->id()
            ));
        }
    }
}
