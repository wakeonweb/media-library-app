    SELECT c.id
      FROM transformed_media AS tm

INNER JOIN media_transformation AS mt
        ON mt.id = tm.media_transformation_id

INNER JOIN media AS m
        ON m.id = mt.media_id

INNER JOIN cdn AS c
        ON c.workspace_id = m.workspace_id

     WHERE tm.id = :id
